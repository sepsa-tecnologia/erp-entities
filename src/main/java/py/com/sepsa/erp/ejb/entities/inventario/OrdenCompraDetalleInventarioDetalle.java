/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "orden_compra_detalle_inventario_detalle", catalog = "erp", schema = "inventario")
@XmlRootElement
public class OrdenCompraDetalleInventarioDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_orden_compra_detalle")
    private Integer idOrdenCompraDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_inventario_detalle")
    private Integer idInventarioDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private Integer cantidad;
    @JoinColumn(name = "id_orden_compra_detalle", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private OrdenCompraDetalle ordenCompraDetalle;
    @JoinColumn(name = "id_inventario_detalle", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private InventarioDetalle inventarioDetalle;

    public OrdenCompraDetalleInventarioDetalle() {
    }

    public OrdenCompraDetalleInventarioDetalle(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdOrdenCompraDetalle() {
        return idOrdenCompraDetalle;
    }

    public void setIdOrdenCompraDetalle(Integer idOrdenCompraDetalle) {
        this.idOrdenCompraDetalle = idOrdenCompraDetalle;
    }

    public Integer getIdInventarioDetalle() {
        return idInventarioDetalle;
    }

    public void setIdInventarioDetalle(Integer idInventarioDetalle) {
        this.idInventarioDetalle = idInventarioDetalle;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public OrdenCompraDetalle getOrdenCompraDetalle() {
        return ordenCompraDetalle;
    }

    public void setOrdenCompraDetalle(OrdenCompraDetalle ordenCompraDetalle) {
        this.ordenCompraDetalle = ordenCompraDetalle;
    }

    public InventarioDetalle getInventarioDetalle() {
        return inventarioDetalle;
    }

    public void setInventarioDetalle(InventarioDetalle inventarioDetalle) {
        this.inventarioDetalle = inventarioDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenCompraDetalleInventarioDetalle)) {
            return false;
        }
        OrdenCompraDetalleInventarioDetalle other = (OrdenCompraDetalleInventarioDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle[ id=" + id + " ]";
    }
    
}
