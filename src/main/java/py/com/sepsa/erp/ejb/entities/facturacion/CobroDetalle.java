/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "cobro_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class CobroDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cobro")
    private Integer idCobro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Basic(optional = false)
    @Column(name = "id_factura")
    private Integer idFactura;
    @Basic(optional = true)
    @NotNull
    @Column(name = "monto_cobro")
    private BigDecimal montoCobro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_cheque")
    private Integer idCheque;
    @Column(name = "id_concepto_cobro")
    private Integer idConceptoCobro;
    @NotNull
    @Column(name = "id_tipo_cobro")
    private Integer idTipoCobro;
    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "monto_cobro_guaranies")
    private BigDecimal montoCobroGuaranies;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_cheque", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Cheque cheque;
    @GsonRepellent
    @JoinColumn(name = "id_cobro", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cobro cobro;
    @JoinColumn(name = "id_concepto_cobro", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private ConceptoCobro conceptoCobro;
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = true)
    private Factura factura;
    @JoinColumn(name = "id_tipo_cobro", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoCobro tipoCobro;
    @JoinColumn(name = "id_entidad_financiera", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Persona entidadFinanciera;

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public Integer getIdConceptoCobro() {
        return idConceptoCobro;
    }

    public void setIdConceptoCobro(Integer idConceptoCobro) {
        this.idConceptoCobro = idConceptoCobro;
    }

    public Integer getIdTipoCobro() {
        return idTipoCobro;
    }

    public void setIdTipoCobro(Integer idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public Cobro getCobro() {
        return cobro;
    }

    public void setCobro(Cobro cobro) {
        this.cobro = cobro;
    }

    public ConceptoCobro getConceptoCobro() {
        return conceptoCobro;
    }

    public void setConceptoCobro(ConceptoCobro conceptoCobro) {
        this.conceptoCobro = conceptoCobro;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public TipoCobro getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(TipoCobro tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    public Persona getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(Persona entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCobro() {
        return idCobro;
    }

    public void setIdCobro(Integer idCobro) {
        this.idCobro = idCobro;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public BigDecimal getMontoCobroGuaranies() {
        return montoCobroGuaranies;
    }

    public void setMontoCobroGuaranies(BigDecimal montoCobroGuaranies) {
        this.montoCobroGuaranies = montoCobroGuaranies;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobroDetalle)) {
            return false;
        }
        CobroDetalle other = (CobroDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.CobroDetalle[ id=" + id + " ]";
    }

}
