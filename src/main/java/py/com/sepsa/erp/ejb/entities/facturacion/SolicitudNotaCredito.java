/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "solicitud_nota_credito", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class SolicitudNotaCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ruc")
    private String ruc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "razon_social")
    private String razonSocial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_documento")
    private String nroDocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "recibido")
    private Character recibido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "archivo_edi")
    private Character archivoEdi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "generado_edi")
    private Character generadoEdi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_5")
    private BigDecimal montoIva5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_5")
    private BigDecimal montoImponible5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_5")
    private BigDecimal montoTotal5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_10")
    private BigDecimal montoIva10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_10")
    private BigDecimal montoImponible10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_10")
    private BigDecimal montoTotal10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_exento")
    private BigDecimal montoTotalExento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_solicitud_nota_credito")
    private BigDecimal montoTotalSolicitudNotaCredito;
    @Column(name = "id_procesamiento_archivo")
    private Integer idProcesamientoArchivo;
    @Column(name = "id_local_origen")
    private Integer idLocalOrigen;
    @Column(name = "id_local_destino")
    private Integer idLocalDestino;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Moneda moneda;
    @JoinColumn(name = "id_local_origen", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localOrigen;
    @JoinColumn(name = "id_local_destino", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localDestino;
    @JoinColumn(name = "id_procesamiento_archivo", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private ProcesamientoArchivo procesamientoArchivo;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = SolicitudNotaCreditoDetalle.class)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSolicitudNotaCredito")
    private Collection<SolicitudNotaCreditoDetalle> solicitudNotaCreditoDetalles;

    public SolicitudNotaCredito() {
    }

    public SolicitudNotaCredito(Integer id) {
        this.id = id;
    }

    public SolicitudNotaCredito(Integer id, Integer idCliente, Integer idMoneda, Date fechaInsercion, Character anulado, Character archivoEdi, Character generadoEdi, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalSolicitudNotaCredito) {
        this.id = id;
        this.idCliente = idCliente;
        this.idMoneda = idMoneda;
        this.fechaInsercion = fechaInsercion;
        this.anulado = anulado;
        this.archivoEdi = archivoEdi;
        this.generadoEdi = generadoEdi;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalSolicitudNotaCredito = montoTotalSolicitudNotaCredito;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public void setMontoTotalSolicitudNotaCredito(BigDecimal montoTotalSolicitudNotaCredito) {
        this.montoTotalSolicitudNotaCredito = montoTotalSolicitudNotaCredito;
    }

    public BigDecimal getMontoTotalSolicitudNotaCredito() {
        return montoTotalSolicitudNotaCredito;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setProcesamientoArchivo(ProcesamientoArchivo procesamientoArchivo) {
        this.procesamientoArchivo = procesamientoArchivo;
    }

    public ProcesamientoArchivo getProcesamientoArchivo() {
        return procesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Character getRecibido() {
        return recibido;
    }

    public void setRecibido(Character recibido) {
        this.recibido = recibido;
    }

    @XmlTransient
    public Collection<SolicitudNotaCreditoDetalle> getSolicitudNotaCreditoDetalles() {
        return solicitudNotaCreditoDetalles;
    }

    public void setSolicitudNotaCreditoDetalles(Collection<SolicitudNotaCreditoDetalle> solicitudNotaCreditoDetalles) {
        this.solicitudNotaCreditoDetalles = solicitudNotaCreditoDetalles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudNotaCredito)) {
            return false;
        }
        SolicitudNotaCredito other = (SolicitudNotaCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.SolicitudNotaCredito[ id=" + id + " ]";
    }
    
}
