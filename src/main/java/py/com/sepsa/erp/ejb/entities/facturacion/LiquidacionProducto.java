/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "liquidacion_producto", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class LiquidacionProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_producto")
    private int idProducto;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "plazo")
    private int plazo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "extension")
    private int extension;
    @Basic(optional = false)
    @NotNull
    @Column(name = "facturas_pendientes")
    private int facturasPendientes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private ProductoCom producto;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne
    private Cliente cliente;

    public LiquidacionProducto() {
    }

    public LiquidacionProducto(Integer id) {
        this.id = id;
    }

    public LiquidacionProducto(Integer id, int idProducto, int plazo, int extension, int facturasPendientes, Character activo) {
        this.id = id;
        this.idProducto = idProducto;
        this.plazo = plazo;
        this.extension = extension;
        this.facturasPendientes = facturasPendientes;
        this.activo = activo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }

    public int getFacturasPendientes() {
        return facturasPendientes;
    }

    public void setFacturasPendientes(int facturasPendientes) {
        this.facturasPendientes = facturasPendientes;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setProducto(ProductoCom producto) {
        this.producto = producto;
    }

    public ProductoCom getProducto() {
        return producto;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionProducto)) {
            return false;
        }
        LiquidacionProducto other = (LiquidacionProducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.LiquidacionProducto[ id=" + id + " ]";
    }
    
}
