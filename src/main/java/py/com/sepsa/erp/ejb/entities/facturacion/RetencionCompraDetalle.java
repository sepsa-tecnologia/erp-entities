/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "retencion_compra_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class RetencionCompraDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible")
    private BigDecimal montoImponible;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva")
    private BigDecimal montoIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_retenido")
    private BigDecimal montoRetenido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura_compra")
    private Integer idFacturaCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_retencion_compra")
    private Integer idRetencionCompra;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_factura_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FacturaCompra facturaCompra;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = RetencionCompra.class)
    @JoinColumn(name = "id_retencion_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private RetencionCompra retencionCompra;

    public RetencionCompraDetalle() {
    }

    public RetencionCompraDetalle(Integer id) {
        this.id = id;
    }

    public RetencionCompraDetalle(Integer id, Integer idEstado, BigDecimal montoImponible, BigDecimal montoIva, BigDecimal montoTotal, BigDecimal montoRetenido) {
        this.id = id;
        this.idEstado = idEstado;
        this.montoImponible = montoImponible;
        this.montoIva = montoIva;
        this.montoTotal = montoTotal;
        this.montoRetenido = montoRetenido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getMontoRetenido() {
        return montoRetenido;
    }

    public void setMontoRetenido(BigDecimal montoRetenido) {
        this.montoRetenido = montoRetenido;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdRetencionCompra() {
        return idRetencionCompra;
    }

    public void setIdRetencionCompra(Integer idRetencionCompra) {
        this.idRetencionCompra = idRetencionCompra;
    }

    public FacturaCompra getFacturaCompra() {
        return facturaCompra;
    }

    public void setFacturaCompra(FacturaCompra facturaCompra) {
        this.facturaCompra = facturaCompra;
    }

    public RetencionCompra getRetencionCompra() {
        return retencionCompra;
    }

    public void setRetencionCompra(RetencionCompra retencionCompra) {
        this.retencionCompra = retencionCompra;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RetencionCompraDetalle)) {
            return false;
        }
        RetencionCompraDetalle other = (RetencionCompraDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompraDetalle[ id=" + id + " ]";
    }
    
}
