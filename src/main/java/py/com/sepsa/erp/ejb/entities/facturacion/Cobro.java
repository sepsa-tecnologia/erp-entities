/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "cobro", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class Cobro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_cobro")
    private BigDecimal montoCobro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_recibo")
    private String nroRecibo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @NotNull
    @Column(name = "digital")
    private Character digital;
    @Column(name = "fecha_envio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Column(name = "enviado")
    private Character enviado;
    @NotNull
    @Column(name = "id_lugar_cobro")
    private Integer idLugarCobro;
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Column(name = "id_tipo_cambio")
    private Integer idTipoCambio;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_transaccion_red")
    private Integer idTransaccionRed;
    @NotNull
    @Column(name = "id_talonario")
    private Integer idTalonario;
    @Column(name = "id_motivo_anulacion")
    private Integer idMotivoAnulacion;
    @Column(name = "observacion_anulacion")
    private String observacionAnulacion;
    @Column(name = "monto_cobro_ficticio")
    private BigDecimal montoCobroFicticio;
    @Column(name = "monto_cobro_guaranies")
    private BigDecimal montoCobroGuaranies;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_lugar_cobro", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private LugarCobro lugarCobro;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Talonario talonario;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_transaccion_red", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private TransaccionRedPago transaccionRed;
    @JoinColumn(name = "id_motivo_anulacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MotivoAnulacion motivoAnulacion;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Moneda moneda;
    @JoinColumn(name = "id_tipo_cambio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private TipoCambio tipoCambio;
    @OneToMany(mappedBy = "cobro")
    private Collection<CobroDetalle> cobroDetalles;

    public Cobro() {
    }

    public Cobro(Integer id) {
        this.id = id;
    }

    public Cobro(Integer id, Date fecha, BigDecimal montoCobro, String nroRecibo, Integer idEstado, Character digital) {
        this.id = id;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.idEstado = idEstado;
        this.digital = digital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Character getEnviado() {
        return enviado;
    }

    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Integer getIdLugarCobro() {
        return idLugarCobro;
    }

    public void setIdLugarCobro(Integer idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    public Integer getIdTransaccionRed() {
        return idTransaccionRed;
    }

    public void setIdTransaccionRed(Integer idTransaccionRed) {
        this.idTransaccionRed = idTransaccionRed;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public LugarCobro getLugarCobro() {
        return lugarCobro;
    }

    public void setLugarCobro(LugarCobro lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public TransaccionRedPago getTransaccionRed() {
        return transaccionRed;
    }

    public void setTransaccionRed(TransaccionRedPago transaccionRed) {
        this.transaccionRed = transaccionRed;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public void setCobroDetalles(Collection<CobroDetalle> cobroDetalles) {
        this.cobroDetalles = cobroDetalles;
    }

    public Collection<CobroDetalle> getCobroDetalles() {
        return cobroDetalles;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public BigDecimal getMontoCobroFicticio() {
        return montoCobroFicticio;
    }

    public void setMontoCobroFicticio(BigDecimal montoCobroFicticio) {
        this.montoCobroFicticio = montoCobroFicticio;
    }

    public BigDecimal getMontoCobroGuaranies() {
        return montoCobroGuaranies;
    }

    public void setMontoCobroGuaranies(BigDecimal montoCobroGuaranies) {
        this.montoCobroGuaranies = montoCobroGuaranies;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cobro)) {
            return false;
        }
        Cobro other = (Cobro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.Cobro[ id=" + id + " ]";
    }

}
