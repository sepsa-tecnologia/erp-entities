/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set;

import py.com.sepsa.erp.ejb.entities.facturacion.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "autofactura_procesamiento", catalog = "erp", schema = "set")
@XmlRootElement
public class AutofacturaProcesamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_autofactura")
    private Integer idAutofactura;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_procesamiento")
    private Integer idProcesamiento;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = AutoFactura.class)
    @JoinColumn(name = "id_autofactura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AutoFactura autofactura;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_procesamiento", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Procesamiento procesamiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAutofactura() {
        return idAutofactura;
    }

    public void setIdAutofactura(Integer idAutofactura) {
        this.idAutofactura = idAutofactura;
    }

    public void setProcesamiento(Procesamiento procesamiento) {
        this.procesamiento = procesamiento;
    }

    public Procesamiento getProcesamiento() {
        return procesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public AutoFactura getAutofactura() {
        return autofactura;
    }

    public void setAutofactura(AutoFactura autofactura) {
        this.autofactura = autofactura;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutofacturaProcesamiento)) {
            return false;
        }
        AutofacturaProcesamiento other = (AutofacturaProcesamiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.FacturaDetalle[ id=" + id + " ]";
    }
    
}
