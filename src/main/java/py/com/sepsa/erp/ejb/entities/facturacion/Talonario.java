/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "talonario", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class Talonario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Size(max = 2147483647)
    @Column(name = "timbrado")
    private String timbrado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inicio")
    private Integer inicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fin")
    private Integer fin;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_sucursal")
    private String nroSucursal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_punto_venta")
    private String nroPuntoVenta;
    @Column(name = "id_encargado")
    private Integer idEncargado;
    @Column(name = "id_local")
    private Integer idLocal;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Size(max = 2147483647)
    @Column(name = "nro_resolucion")
    private String nroResolucion;
    @Column(name = "fecha_resolucion")
    @Temporal(TemporalType.DATE)
    private Date fechaResolucion;
    @Size(max = 2147483647)
    @Column(name = "serie")
    private String serie;
    @Basic(optional = false)
    @NotNull
    @Column(name = "digital")
    private Character digital;
    @Column(name = "codigo_interno")
    private Integer codigoInterno;
    @NotNull
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDocumento tipoDocumento;
    @JoinColumn(name = "id_encargado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Persona encargado;
    @JoinColumn(name = "id_local", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Local local;
    @GsonRepellent
    @OneToMany(mappedBy = "talonario")
    private Collection<TalonarioLocal> talonarioLocales;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;

    public Talonario() {
    }

    public Talonario(Integer id) {
        this.id = id;
    }

    public Talonario(Integer id, int inicio, int fin, Character activo, String nroSucursal, String nroPuntoVenta, Character digital) {
        this.id = id;
        this.inicio = inicio;
        this.fin = fin;
        this.activo = activo;
        this.nroSucursal = nroSucursal;
        this.nroPuntoVenta = nroPuntoVenta;
        this.digital = digital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public Integer getInicio() {
        return inicio;
    }

    public void setInicio(Integer inicio) {
        this.inicio = inicio;
    }

    public Integer getFin() {
        return fin;
    }

    public void setFin(Integer fin) {
        this.fin = fin;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public String getNroSucursal() {
        return nroSucursal;
    }

    public void setNroSucursal(String nroSucursal) {
        this.nroSucursal = nroSucursal;
    }

    public String getNroPuntoVenta() {
        return nroPuntoVenta;
    }

    public void setNroPuntoVenta(String nroPuntoVenta) {
        this.nroPuntoVenta = nroPuntoVenta;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getNroResolucion() {
        return nroResolucion;
    }

    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Persona getEncargado() {
        return encargado;
    }

    public void setEncargado(Persona encargado) {
        this.encargado = encargado;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Local getLocal() {
        return local;
    }

    public void setTalonarioLocales(Collection<TalonarioLocal> talonarioLocales) {
        this.talonarioLocales = talonarioLocales;
    }

    public Collection<TalonarioLocal> getTalonarioLocales() {
        return talonarioLocales;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Integer getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(Integer codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Talonario)) {
            return false;
        }
        Talonario other = (Talonario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.Talonario[ id=" + id + " ]";
    }
    
}
