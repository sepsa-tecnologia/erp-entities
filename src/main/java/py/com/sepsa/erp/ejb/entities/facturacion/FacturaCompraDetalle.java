/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "factura_compra_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class FacturaCompraDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_iva")
    private Integer porcentajeIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad_facturada")
    private Integer cantidadFacturada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_sin_iva")
    private BigDecimal precioUnitarioSinIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_con_iva")
    private BigDecimal precioUnitarioConIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva")
    private BigDecimal montoIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible")
    private BigDecimal montoImponible;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura_compra")
    private Integer idFacturaCompra;
    @Column(name = "id_producto")
    private Integer idProducto;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = FacturaCompra.class)
    @JoinColumn(name = "id_factura_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FacturaCompra facturaCompra;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Producto producto;

    public FacturaCompraDetalle() {
    }

    public FacturaCompraDetalle(Integer id) {
        this.id = id;
    }

    public FacturaCompraDetalle(Integer id, Integer nroLinea, String descripcion, Integer porcentajeIva, Integer cantidadFacturada, BigDecimal precioUnitarioSinIva, BigDecimal precioUnitarioConIva, BigDecimal montoIva, BigDecimal montoImponible, BigDecimal montoTotal) {
        this.id = id;
        this.nroLinea = nroLinea;
        this.descripcion = descripcion;
        this.porcentajeIva = porcentajeIva;
        this.cantidadFacturada = cantidadFacturada;
        this.precioUnitarioSinIva = precioUnitarioSinIva;
        this.precioUnitarioConIva = precioUnitarioConIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public void setCantidadFacturada(Integer cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }

    public Integer getCantidadFacturada() {
        return cantidadFacturada;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public FacturaCompra getFacturaCompra() {
        return facturaCompra;
    }

    public void setFacturaCompra(FacturaCompra facturaCompra) {
        this.facturaCompra = facturaCompra;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacturaCompraDetalle)) {
            return false;
        }
        FacturaCompraDetalle other = (FacturaCompraDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompraDetalle[ id=" + id + " ]";
    }
    
}
