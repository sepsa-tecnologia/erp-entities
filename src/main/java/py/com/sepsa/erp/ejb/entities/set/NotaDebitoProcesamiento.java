/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set;

import py.com.sepsa.erp.ejb.entities.facturacion.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "nota_debito_procesamiento", catalog = "erp", schema = "set")
@XmlRootElement
public class NotaDebitoProcesamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_nota_debito")
    private Integer idNotaDebito;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_procesamiento")
    private Integer idProcesamiento;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = NotaDebito.class)
    @JoinColumn(name = "id_nota_debito", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NotaDebito notaDebito;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_procesamiento", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Procesamiento procesamiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdNotaDebito(Integer idNotaDebito) {
        this.idNotaDebito = idNotaDebito;
    }

    public Integer getIdNotaDebito() {
        return idNotaDebito;
    }

    public void setProcesamiento(Procesamiento procesamiento) {
        this.procesamiento = procesamiento;
    }

    public Procesamiento getProcesamiento() {
        return procesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setNotaDebito(NotaDebito notaDebito) {
        this.notaDebito = notaDebito;
    }

    public NotaDebito getNotaDebito() {
        return notaDebito;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaDebitoProcesamiento)) {
            return false;
        }
        NotaDebitoProcesamiento other = (NotaDebitoProcesamiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.FacturaDetalle[ id=" + id + " ]";
    }
    
}
