/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario;

import py.com.sepsa.erp.ejb.entities.info.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "control_inventario_detalle", catalog = "erp", schema = "inventario")
@XmlRootElement
public class ControlInventarioDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_control_inventario")
    private Integer idControlInventario;
    @NotNull
    @Column(name = "id_deposito_logistico")
    private Integer idDepositoLogistico;
    @NotNull
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "observacion")
    private String observacion;
    @NotNull
    @Column(name = "cantidad_inicial")
    private Integer cantidadInicial;
    @Column(name = "cantidad_final")
    private Integer cantidadFinal;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = ControlInventario.class)
    @JoinColumn(name = "id_control_inventario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ControlInventario controlInventario;
    @JoinColumn(name = "id_deposito_logistico", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private DepositoLogistico depositoLogistico;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;

    public ControlInventarioDetalle() {
    }

    public ControlInventarioDetalle(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public ControlInventario getControlInventario() {
        return controlInventario;
    }

    public void setControlInventario(ControlInventario controlInventario) {
        this.controlInventario = controlInventario;
    }

    public DepositoLogistico getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(DepositoLogistico depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ControlInventarioDetalle)) {
            return false;
        }
        ControlInventarioDetalle other = (ControlInventarioDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.inventario.OperacionInventarioDetalle[ id=" + id + " ]";
    }

}
