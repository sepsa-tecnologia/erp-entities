/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "referencia_geografica", catalog = "erp", schema = "info")
@XmlRootElement
public class ReferenciaGeografica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_referencia_geografica")
    private Integer idTipoReferenciaGeografica;
    @Column(name = "id_padre")
    private Integer idPadre;
    @JoinColumn(name = "id_tipo_referencia_geografica", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoReferenciaGeografica tipoReferenciaGeografica;
    @JoinColumn(name = "id_padre", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private ReferenciaGeografica padre;

    public ReferenciaGeografica() {
    }

    public ReferenciaGeografica(Integer id) {
        this.id = id;
    }

    public ReferenciaGeografica(Integer id, String codigo, String descripcion, Integer idTipoReferenciaGeografica) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.idTipoReferenciaGeografica = idTipoReferenciaGeografica;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setTipoReferenciaGeografica(TipoReferenciaGeografica tipoReferenciaGeografica) {
        this.tipoReferenciaGeografica = tipoReferenciaGeografica;
    }

    public TipoReferenciaGeografica getTipoReferenciaGeografica() {
        return tipoReferenciaGeografica;
    }

    public void setIdTipoReferenciaGeografica(Integer idTipoReferenciaGeografica) {
        this.idTipoReferenciaGeografica = idTipoReferenciaGeografica;
    }

    public Integer getIdTipoReferenciaGeografica() {
        return idTipoReferenciaGeografica;
    }

    public void setPadre(ReferenciaGeografica padre) {
        this.padre = padre;
    }

    public ReferenciaGeografica getPadre() {
        return padre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public Integer getIdPadre() {
        return idPadre;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReferenciaGeografica)) {
            return false;
        }
        ReferenciaGeografica other = (ReferenciaGeografica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.ReferenciaGeografica[ id=" + id + " ]";
    }
    
}
