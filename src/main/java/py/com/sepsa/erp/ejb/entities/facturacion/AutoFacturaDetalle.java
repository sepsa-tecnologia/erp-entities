/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "autofactura_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class AutoFacturaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_autofactura")
    private Integer idAutofactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "dato_adicional")
    private String datoAdicional;
    @Column(name = "cod_dncp_nivel_general")
    private String codDncpNivelGeneral;
    @Column(name = "cod_dncp_nivel_especifico")
    private String codDncpNivelEspecifico;
    @Column(name = "porcentaje_gravada")
    private Integer porcentajeGravada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private BigDecimal cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario")
    private BigDecimal precioUnitario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descuento_particular_unitario")
    private BigDecimal descuentoParticularUnitario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descuento_global_unitario")
    private BigDecimal descuentoGlobalUnitario;
    @Column(name = "monto_exento_gravado")
    private BigDecimal montoExentoGravado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_particular")
    private BigDecimal montoDescuentoParticular;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_global")
    private BigDecimal montoDescuentoGlobal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = AutoFactura.class)
    @JoinColumn(name = "id_autofactura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AutoFactura autoFactura;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAutofactura() {
        return idAutofactura;
    }

    public void setIdAutofactura(Integer idAutofactura) {
        this.idAutofactura = idAutofactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setPorcentajeGravada(Integer porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public Integer getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public AutoFactura getAutoFactura() {
        return autoFactura;
    }

    public void setAutoFactura(AutoFactura autoFactura) {
        this.autoFactura = autoFactura;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }    

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutoFacturaDetalle)) {
            return false;
        }
        AutoFacturaDetalle other = (AutoFacturaDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.FacturaDetalle[ id=" + id + " ]";
    }
    
}
