/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "solicitud_nota_credito_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class SolicitudNotaCreditoDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Column(name = "id_factura")
    private Integer idFactura;
    @Column(name = "id_factura_compra")
    private Integer idFacturaCompra;
    @Column(name = "id_producto")
    @Basic(optional = false)
    @NotNull
    private Integer idProducto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_solicitud_nota_credito")
    private Integer idSolicitudNotaCredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_iva")
    private Integer porcentajeIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private Integer cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_con_iva")
    private BigDecimal precioUnitarioConIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_sin_iva")
    private BigDecimal precioUnitarioSinIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva")
    private BigDecimal montoIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible")
    private BigDecimal montoImponible;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_motivo_emision_snc")
    private Integer idMotivoEmisionSnc;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "id_motivo_emision_snc", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MotivoEmisionSnc motivoEmisionSnc;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "id_factura_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private FacturaCompra facturaCompra;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = SolicitudNotaCredito.class)
    @JoinColumn(name = "id_solicitud_nota_credito", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SolicitudNotaCredito solicitudNotaCredito;

    public SolicitudNotaCreditoDetalle() {
    }

    public SolicitudNotaCreditoDetalle(Integer id) {
        this.id = id;
    }

    public SolicitudNotaCreditoDetalle(Integer id, Integer nroLinea, Integer porcentajeIva, BigDecimal montoIva, BigDecimal montoImponible, BigDecimal montoTotal, Integer idMotivoEmisionSnc) {
        this.id = id;
        this.nroLinea = nroLinea;
        this.porcentajeIva = porcentajeIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
        this.idMotivoEmisionSnc = idMotivoEmisionSnc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setMotivoEmisionSnc(MotivoEmisionSnc motivoEmisionSnc) {
        this.motivoEmisionSnc = motivoEmisionSnc;
    }

    public MotivoEmisionSnc getMotivoEmisionSnc() {
        return motivoEmisionSnc;
    }

    public void setIdMotivoEmisionSnc(Integer idMotivoEmisionSnc) {
        this.idMotivoEmisionSnc = idMotivoEmisionSnc;
    }

    public Integer getIdMotivoEmisionSnc() {
        return idMotivoEmisionSnc;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdSolicitudNotaCredito() {
        return idSolicitudNotaCredito;
    }

    public void setIdSolicitudNotaCredito(Integer idSolicitudNotaCredito) {
        this.idSolicitudNotaCredito = idSolicitudNotaCredito;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public SolicitudNotaCredito getSolicitudNotaCredito() {
        return solicitudNotaCredito;
    }

    public void setSolicitudNotaCredito(SolicitudNotaCredito solicitudNotaCredito) {
        this.solicitudNotaCredito = solicitudNotaCredito;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setFacturaCompra(FacturaCompra facturaCompra) {
        this.facturaCompra = facturaCompra;
    }

    public FacturaCompra getFacturaCompra() {
        return facturaCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudNotaCreditoDetalle)) {
            return false;
        }
        SolicitudNotaCreditoDetalle other = (SolicitudNotaCreditoDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.SolicitudNotaCreditoDetalle[ id=" + id + " ]";
    }
    
}
