/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.auditoria;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "valor_columna", catalog = "erp", schema = "auditoria")
@XmlRootElement
public class ValorColumna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_auditoria")
    private Integer idAuditoria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_auditoria_detalle")
    private Integer idAuditoriaDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nombre_columna")
    private String nombreColumna;
    @Column(name = "valor_anterior")
    private String valorAnterior;
    @Column(name = "valor_nuevo")
    private String valorNuevo;

    public ValorColumna() {
    }

    public ValorColumna(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAuditoriaDetalle() {
        return idAuditoriaDetalle;
    }

    public void setIdAuditoriaDetalle(Integer idAuditoriaDetalle) {
        this.idAuditoriaDetalle = idAuditoriaDetalle;
    }

    public String getValorAnterior() {
        return valorAnterior;
    }

    public void setValorAnterior(String valorAnterior) {
        this.valorAnterior = valorAnterior;
    }

    public String getValorNuevo() {
        return valorNuevo;
    }

    public void setValorNuevo(String valorNuevo) {
        this.valorNuevo = valorNuevo;
    }

    public void setNombreColumna(String nombreColumna) {
        this.nombreColumna = nombreColumna;
    }

    public String getNombreColumna() {
        return nombreColumna;
    }

    public void setIdAuditoria(Integer idAuditoria) {
        this.idAuditoria = idAuditoria;
    }

    public Integer getIdAuditoria() {
        return idAuditoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorColumna)) {
            return false;
        }
        ValorColumna other = (ValorColumna) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.auditoria.ValorColumna[ id=" + id + " ]";
    }
    
}
