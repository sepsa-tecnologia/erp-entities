/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "procesamiento", catalog = "erp", schema = "set")
@XmlRootElement
public class Procesamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_envio")
    private BigInteger idEnvio;
    @Column(name = "nro_transaccion")
    private BigInteger nroTransaccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_procesamiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProcesamiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "estado_resultado")
    private String estadoResultado;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = DetalleProcesamiento.class)
    @OneToMany(mappedBy = "procesamiento")
    private Collection<DetalleProcesamiento> detalleProcesamientos;

    public Procesamiento() {
    }

    public Procesamiento(Integer id) {
        this.id = id;
    }

    public Procesamiento(Integer id, BigInteger idEnvio, Date fechaInsercion, Date fechaProcesamiento, String estadoResultado) {
        this.id = id;
        this.idEnvio = idEnvio;
        this.fechaInsercion = fechaInsercion;
        this.fechaProcesamiento = fechaProcesamiento;
        this.estadoResultado = estadoResultado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(BigInteger idEnvio) {
        this.idEnvio = idEnvio;
    }

    public BigInteger getNroTransaccion() {
        return nroTransaccion;
    }

    public void setNroTransaccion(BigInteger nroTransaccion) {
        this.nroTransaccion = nroTransaccion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public String getEstadoResultado() {
        return estadoResultado;
    }

    public void setEstadoResultado(String estadoResultado) {
        this.estadoResultado = estadoResultado;
    }

    public void setDetalleProcesamientos(Collection<DetalleProcesamiento> detalleProcesamientos) {
        this.detalleProcesamientos = detalleProcesamientos;
    }

    public Collection<DetalleProcesamiento> getDetalleProcesamientos() {
        return detalleProcesamientos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procesamiento)) {
            return false;
        }
        Procesamiento other = (Procesamiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.setserver.ejb.entities.trans.Procesamiento[ id=" + id + " ]";
    }
    
}
