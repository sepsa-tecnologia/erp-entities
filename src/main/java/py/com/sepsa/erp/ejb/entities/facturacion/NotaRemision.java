/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "nota_remision", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class NotaRemision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @NotNull
    @Column(name = "id_naturaleza_cliente")
    private Integer idNaturalezaCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_nota_remision")
    private String nroNotaRemision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social")
    private String razonSocial;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "ruc")
    private String ruc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anulado")
    private Character anulado;
    @Column(name = "digital")
    private Character digital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado_sincronizado")
    private Character estadoSincronizado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "generado_set")
    private Character generadoSet;
    @Column(name = "archivo_set")
    private Character archivoSet;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_talonario")
    private Integer idTalonario;
    @Column(name = "id_motivo_emision_nr")
    private Integer idMotivoEmisionNr;
    @Column(name = "descripcion_motivo_emision")
    private String descripcionMotivoEmisionNr;
    @Size(max = 2147483647)
    @Column(name = "cdc")
    private String cdc;
    @Size(max = 2147483647)
    @Column(name = "cod_seguridad")
    private String codSeguridad;
    @Column(name = "nro_casa")
    private String nroCasa;
    @Column(name = "id_departamento")
    private Integer idDepartamento;
    @Column(name = "id_distrito")
    private Integer idDistrito;
    @Column(name = "id_ciudad")
    private Integer idCiudad;
    @Size(max = 2147483647)
    @Column(name = "serie")
    private String serie;
    @Column(name = "id_procesamiento")
    private Integer idProcesamiento;
    @Column(name = "km")
    private BigDecimal km;
    @Column(name = "id_local_salida")
    private Integer idLocalSalida;
    @Column(name = "id_local_entrega")
    private Integer idLocalEntrega;
    @Column(name = "id_transportista")
    private Integer idTransportista;
    @Column(name = "id_vehiculo_traslado")
    private Integer idVehiculoTraslado;
    @Column(name = "tipo_transporte")
    private Integer tipoTransporte;
    @Column(name = "modalidad_transporte")
    private Integer modalidadTransporte;
    @Column(name = "responsable_costo_flete")
    private Integer responsableCostoFlete;
    @Column(name = "nro_despacho_importacion")
    private String nroDespachoImportacion;
    @Column(name = "fecha_inicio_traslado")
    private Date fechaInicioTraslado;
    @Column(name = "fecha_fin_traslado")
    private Date fechaFinTraslado;    
    @Column(name = "fecha_futura_emision")
    private Date fechaFuturaEmision;  
    @Column(name = "responsable_emision")
    private Integer responsableEmision;
    @Column(name = "cdc_factura")
    private String cdcFactura;
    @Column(name = "timbrado_factura")
    private String timbradoFactura;
    @Column(name = "nro_factura")
    private String nroFactura;
    @Column(name = "fecha_factura")
    private Date fechaFactura;
    @Column(name = "tiene_factura")
    private Character tieneFactura;
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_motivo_anulacion")
    private Integer idMotivoAnulacion;
    @Column(name = "observacion_anulacion")
    private String observacionAnulacion;
    @JoinColumn(name = "id_motivo_emision_nr", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MotivoEmisionNr motivoEmisionNr;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Talonario talonario;
    @JoinColumn(name = "id_local_salida", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private LocalSalida localSalida;
    @JoinColumn(name = "id_local_entrega", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private LocalEntrega localEntrega;
    @JoinColumn(name = "id_transportista", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Transportista transportista;
    @JoinColumn(name = "id_vehiculo_traslado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private VehiculoTraslado vehiculoTraslado;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Estado estado;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = NotaRemisionDetalle.class)
    @OneToMany(mappedBy = "notaRemision")
    private Collection<NotaRemisionDetalle> notaRemisionDetalles;

    public NotaRemision() {
    }

    public NotaRemision(Integer id) {
        this.id = id;
    }

    public NotaRemision(Integer id, String nroNotaRemision, Date fecha, String razonSocial, Character anulado,Integer idMotivoEmisionNr) {
        this.id = id;
        this.nroNotaRemision = nroNotaRemision;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.anulado = anulado;
        this.idMotivoEmisionNr = idMotivoEmisionNr;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public Collection<NotaRemisionDetalle> getNotaRemisionDetalles() {
        return notaRemisionDetalles;
    }

    public void setNotaRemisionDetalles(Collection<NotaRemisionDetalle> notaRemisionDetalles) {
        this.notaRemisionDetalles = notaRemisionDetalles;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }


    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }
    
    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }


    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }


    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Integer getIdMotivoEmisionNr() {
        return idMotivoEmisionNr;
    }

    public void setIdMotivoEmisionNr(Integer idMotivoEmisionNr) {
        this.idMotivoEmisionNr = idMotivoEmisionNr;
    }

    public String getDescripcionMotivoEmisionNr() {
        return descripcionMotivoEmisionNr;
    }

    public void setDescripcionMotivoEmisionNr(String descripcionMotivoEmisionNr) {
        this.descripcionMotivoEmisionNr = descripcionMotivoEmisionNr;
    }

    public BigDecimal getKm() {
        return km;
    }

    public void setKm(BigDecimal km) {
        this.km = km;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Integer getIdLocalSalida() {
        return idLocalSalida;
    }

    public void setIdLocalSalida(Integer idLocalSalida) {
        this.idLocalSalida = idLocalSalida;
    }

    public Integer getIdLocalEntrega() {
        return idLocalEntrega;
    }

    public void setIdLocalEntrega(Integer idLocalEntrega) {
        this.idLocalEntrega = idLocalEntrega;
    }

    public Integer getIdTransportista() {
        return idTransportista;
    }

    public void setIdTransportista(Integer idTransportista) {
        this.idTransportista = idTransportista;
    }

    public Integer getIdVehiculoTraslado() {
        return idVehiculoTraslado;
    }

    public void setIdVehiculoTraslado(Integer idVehiculoTraslado) {
        this.idVehiculoTraslado = idVehiculoTraslado;
    }

    public LocalSalida getLocalSalida() {
        return localSalida;
    }

    public void setLocalSalida(LocalSalida localSalida) {
        this.localSalida = localSalida;
    }

    public LocalEntrega getLocalEntrega() {
        return localEntrega;
    }

    public void setLocalEntrega(LocalEntrega localEntrega) {
        this.localEntrega = localEntrega;
    }

    public Transportista getTransportista() {
        return transportista;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    public VehiculoTraslado getVehiculoTraslado() {
        return vehiculoTraslado;
    }

    public void setVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {
        this.vehiculoTraslado = vehiculoTraslado;
    }

    public Integer getTipoTransporte() {
        return tipoTransporte;
    }

    public void setTipoTransporte(Integer tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }

    public Integer getModalidadTransporte() {
        return modalidadTransporte;
    }

    public void setModalidadTransporte(Integer modalidadTransporte) {
        this.modalidadTransporte = modalidadTransporte;
    }

    public Integer getResponsableCostoFlete() {
        return responsableCostoFlete;
    }

    public void setResponsableCostoFlete(Integer responsableCostoFlete) {
        this.responsableCostoFlete = responsableCostoFlete;
    }

    public String getNroDespachoImportacion() {
        return nroDespachoImportacion;
    }

    public void setNroDespachoImportacion(String nroDespachoImportacion) {
        this.nroDespachoImportacion = nroDespachoImportacion;
    }

    public Date getFechaInicioTraslado() {
        return fechaInicioTraslado;
    }

    public void setFechaInicioTraslado(Date fechaInicioTraslado) {
        this.fechaInicioTraslado = fechaInicioTraslado;
    }

    public Date getFechaFinTraslado() {
        return fechaFinTraslado;
    }

    public void setFechaFinTraslado(Date fechaFinTraslado) {
        this.fechaFinTraslado = fechaFinTraslado;
    }

    public Integer getResponsableEmision() {
        return responsableEmision;
    }

    public void setResponsableEmision(Integer responsableEmision) {
        this.responsableEmision = responsableEmision;
    }

    public MotivoEmisionNr getMotivoEmisionNr() {
        return motivoEmisionNr;
    }

    public void setMotivoEmisionNr(MotivoEmisionNr motivoEmisionNr) {
        this.motivoEmisionNr = motivoEmisionNr;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public Date getFechaFuturaEmision() {
        return fechaFuturaEmision;
    }

    public void setFechaFuturaEmision(Date fechaFuturaEmision) {
        this.fechaFuturaEmision = fechaFuturaEmision;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public Character getTieneFactura() {
        return tieneFactura;
    }

    public void setTieneFactura(Character tieneFactura) {
        this.tieneFactura = tieneFactura;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaRemision)) {
            return false;
        }
        NotaRemision other = (NotaRemision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.NotaRemision[ id=" + id + " ]";
    }
    
}
