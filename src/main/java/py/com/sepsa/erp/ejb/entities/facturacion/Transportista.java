/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "transportista", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class Transportista implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_naturaleza_transportista")
    private Integer idNaturalezaTransportista;
    @NotNull
    @Column(name = "razon_social")
    private String razonSocial;
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "verificador_ruc")
    private Integer verificadorRuc;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Column(name = "nro_documento")
    private String nroDocumento;
    @Column(name = "nombre_completo")
    private String nombreCompleto;
    @Column(name = "domicilio_fiscal")
    private String domicilioFiscal;
    @Column(name = "direccion_chofer")
    private String direccionChofer;
    @Column(name = "nro_documento_chofer")
    private String nroDocumentoChofer;
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_naturaleza_transportista", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private NaturalezaTransportista naturalezaTransportista;
    
    public Transportista() {
    }

    public Transportista(Integer id) {
        this.id = id;
    }

    public Transportista(Integer id, Integer idNaturalezaTransportista, String ruc, String razon ) {
        this.id = id;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdNaturalezaTransportista() {
        return idNaturalezaTransportista;
    }

    public void setIdNaturalezaTransportista(Integer idNaturalezaTransportista) {
        this.idNaturalezaTransportista = idNaturalezaTransportista;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getVerificadorRuc() {
        return verificadorRuc;
    }

    public void setVerificadorRuc(Integer verificadorRuc) {
        this.verificadorRuc = verificadorRuc;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDireccionChofer() {
        return direccionChofer;
    }

    public void setDireccionChofer(String direccionChofer) {
        this.direccionChofer = direccionChofer;
    }

    public NaturalezaTransportista getNaturalezaTransportista() {
        return naturalezaTransportista;
    }

    public void setNaturalezaTransportista(NaturalezaTransportista naturalezaTransportista) {
        this.naturalezaTransportista = naturalezaTransportista;
    }

    public String getNroDocumentoChofer() {
        return nroDocumentoChofer;
    }

    public void setNroDocumentoChofer(String nroDocumentoChofer) {
        this.nroDocumentoChofer = nroDocumentoChofer;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    } 
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transportista)) {
            return false;
        }
        Transportista other = (Transportista) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.Transportista[ id=" + id + " ]";
    }
    
}
