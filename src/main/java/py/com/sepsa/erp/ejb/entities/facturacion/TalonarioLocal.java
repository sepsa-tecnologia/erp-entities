/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import py.com.sepsa.erp.ejb.entities.usuario.*;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "talonario_local", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class TalonarioLocal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_local")
    private Integer idLocal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_talonario")
    private Integer idTalonario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = Usuario.class)
    @JoinColumn(name = "id_talonario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Talonario talonario;
    @JoinColumn(name = "id_local", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Local local;

    public TalonarioLocal() {
    }

    public TalonarioLocal(Integer id) {
        this.id = id;
    }

    public TalonarioLocal(Integer id, Integer idTalonario, int idLocal, Character activo) {
        this.id = id;
        this.idTalonario = idTalonario;
        this.idLocal = idLocal;
        this.activo = activo;
    }

    public TalonarioLocal(Integer idTalonario, int idLocal, Character activo) {
        this.idTalonario = idTalonario;
        this.idLocal = idLocal;
        this.activo = activo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
        this.local = new Local(idLocal);
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TalonarioLocal)) {
            return false;
        }
        TalonarioLocal other = (TalonarioLocal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.usuario.UsuarioLocal[ id=" + id + " ]";
    }
    
}
