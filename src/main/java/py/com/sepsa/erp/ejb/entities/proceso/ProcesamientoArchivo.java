/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "procesamiento_archivo", catalog = "erp", schema = "proceso")
@XmlRootElement
public class ProcesamientoArchivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tamano")
    private Integer tamano;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "hash")
    private String hash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_archivo")
    private Integer idTipoArchivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmado")
    private Character confirmado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reenviado")
    private Character reenviado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Column(name = "id_local_origen")
    private Integer idLocalOrigen;
    @Column(name = "id_local_destino")
    private Integer idLocalDestino;
    @Size(max = 2147483647)
    @Column(name = "ruta_archivo_erroneo")
    private String rutaArchivoErroneo;
    @Size(max = 2147483647)
    @Column(name = "nro_documento")
    private String nroDocumento;
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @JoinColumn(name = "id_tipo_archivo", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoArchivo tipoArchivo;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDocumentoProceso tipoDocumento;
    @JoinColumn(name = "id_local_origen", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Local localOrigen;
    @JoinColumn(name = "id_local_destino", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Local localDestino;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = ProcesamientoArchivoDetalle.class)
    @OneToMany(mappedBy = "procesamientoArchivo")
    private Collection<ProcesamientoArchivoDetalle> procesamientoArchivoDetalles;

    public ProcesamientoArchivo() {
    }

    public ProcesamientoArchivo(Integer id) {
        this.id = id;
    }

    public ProcesamientoArchivo(Integer id, String nombreArchivo, Date fechaInsercion, String hash, int idEstado) {
        this.id = id;
        this.nombreArchivo = nombreArchivo;
        this.fechaInsercion = fechaInsercion;
        this.hash = hash;
        this.idEstado = idEstado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setConfirmado(Character confirmado) {
        this.confirmado = confirmado;
    }

    public Character getConfirmado() {
        return confirmado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getRutaArchivoErroneo() {
        return rutaArchivoErroneo;
    }

    public void setRutaArchivoErroneo(String rutaArchivoErroneo) {
        this.rutaArchivoErroneo = rutaArchivoErroneo;
    }

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public TipoArchivo getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(TipoArchivo tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setTamano(Integer tamano) {
        this.tamano = tamano;
    }

    public Integer getTamano() {
        return tamano;
    }

    public Character getReenviado() {
        return reenviado;
    }

    public void setReenviado(Character reenviado) {
        this.reenviado = reenviado;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public void setTipoDocumento(TipoDocumentoProceso tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public TipoDocumentoProceso getTipoDocumento() {
        return tipoDocumento;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public Collection<ProcesamientoArchivoDetalle> getProcesamientoArchivoDetalles() {
        return procesamientoArchivoDetalles;
    }

    public void setProcesamientoArchivoDetalles(Collection<ProcesamientoArchivoDetalle> procesamientoArchivoDetalles) {
        this.procesamientoArchivoDetalles = procesamientoArchivoDetalles;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcesamientoArchivo)) {
            return false;
        }
        ProcesamientoArchivo other = (ProcesamientoArchivo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo[ id=" + id + " ]";
    }
    
}
