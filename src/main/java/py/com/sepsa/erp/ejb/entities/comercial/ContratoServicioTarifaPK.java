/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jonathan
 */
@Embeddable
public class ContratoServicioTarifaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_contrato")
    private int idContrato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_servicio")
    private int idServicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tarifa")
    private int idTarifa;

    public ContratoServicioTarifaPK() {
    }

    public ContratoServicioTarifaPK(int idContrato, int idServicio, int idTarifa) {
        this.idContrato = idContrato;
        this.idServicio = idServicio;
        this.idTarifa = idTarifa;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public int getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(int idTarifa) {
        this.idTarifa = idTarifa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContrato;
        hash += (int) idServicio;
        hash += (int) idTarifa;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoServicioTarifaPK)) {
            return false;
        }
        ContratoServicioTarifaPK other = (ContratoServicioTarifaPK) object;
        if (this.idContrato != other.idContrato) {
            return false;
        }
        if (this.idServicio != other.idServicio) {
            return false;
        }
        if (this.idTarifa != other.idTarifa) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.ContratoServicioTarifaPK[ idContrato=" + idContrato + ", idServicio=" + idServicio + ", idTarifa=" + idTarifa + " ]";
    }
    
}
