/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "parametro_adicional", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class ParametroAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descripcion")
    private String descripcion;
    @NotNull
    @Column(name = "codigo_tipo_parametro_adicional")
    private String codigoTipoParametroAdicional;
    @NotNull
    @Column(name = "codigo_tipo_dato_parametro_adicional")
    private String codigoTipoDatoParametroAdicional;
    @NotNull
    @Column(name = "mandatorio")
    private Character mandatorio;
    @NotNull
    @Column(name = "visible")
    private Character visible;
    @Column(name = "valor_defecto")
    private String valorDefecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @Column(name = "datos_adicionales")
    private String datosAdicionales; 
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "nombre_clave")
    private String nombreClave;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;


    public ParametroAdicional() {
    }

    public ParametroAdicional(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoTipoParametroAdicional() {
        return codigoTipoParametroAdicional;
    }

    public void setCodigoTipoParametroAdicional(String codigoTipoParametroAdicional) {
        this.codigoTipoParametroAdicional = codigoTipoParametroAdicional;
    }

    public Character getMandatorio() {
        return mandatorio;
    }

    public void setMandatorio(Character mandatorio) {
        this.mandatorio = mandatorio;
    }

    public Character getVisible() {
        return visible;
    }

    public void setVisible(Character visible) {
        this.visible = visible;
    }

    public String getValorDefecto() {
        return valorDefecto;
    }

    public void setValorDefecto(String valorDefecto) {
        this.valorDefecto = valorDefecto;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public String getDatosAdicionales() {
        return datosAdicionales;
    }

    public void setDatosAdicionales(String datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getCodigoTipoDatoParametroAdicional() {
        return codigoTipoDatoParametroAdicional;
    }

    public void setCodigoTipoDatoParametroAdicional(String codigoTipoDatoParametroAdicional) {
        this.codigoTipoDatoParametroAdicional = codigoTipoDatoParametroAdicional;
    }

    public String getNombreClave() {
        return nombreClave;
    }

    public void setNombreClave(String nombreClave) {
        this.nombreClave = nombreClave;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParametroAdicional)) {
            return false;
        }
        ParametroAdicional other = (ParametroAdicional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.ParametroAdicional[ id=" + id + " ]";
    }

}
