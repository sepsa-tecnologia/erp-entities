/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "nota_credito_parametro_adicional", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class NotaCreditoParametroAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @NotNull
    @Column(name = "id_nota_credito")
    private Integer idNotaCredito;
    @NotNull
    @Column(name = "id_parametro_adicional")
    private Integer idParametroAdicional;
    @Column(name = "valor")
    private String valor;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_nota_credito", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private NotaCredito notaCredito;
    @JoinColumn(name = "id_parametro_adicional", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private ParametroAdicional parametroAdicional;


    public NotaCreditoParametroAdicional() {
    }

    public NotaCreditoParametroAdicional(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setIdNotaCredito(Integer idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public Integer getIdParametroAdicional() {
        return idParametroAdicional;
    }

    public void setIdParametroAdicional(Integer idParametroAdicional) {
        this.idParametroAdicional = idParametroAdicional;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    public NotaCredito getNotaCredito() {
        return notaCredito;
    }

    public void setNotaCredito(NotaCredito notaCredito) {
        this.notaCredito = notaCredito;
    }

    public ParametroAdicional getParametroAdicional() {
        return parametroAdicional;
    }

    public void setParametroAdicional(ParametroAdicional parametroAdicional) {
        this.parametroAdicional = parametroAdicional;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

   
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaCreditoParametroAdicional)) {
            return false;
        }
        NotaCreditoParametroAdicional other = (NotaCreditoParametroAdicional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.ParametroAdicional[ id=" + id + " ]";
    }

}
