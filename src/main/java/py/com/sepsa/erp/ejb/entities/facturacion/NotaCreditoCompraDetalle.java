/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "nota_credito_compra_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class NotaCreditoCompraDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_iva")
    private Integer porcentajeIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private Integer cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_con_iva")
    private BigDecimal precioUnitarioConIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_sin_iva")
    private BigDecimal precioUnitarioSinIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva")
    private BigDecimal montoIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible")
    private BigDecimal montoImponible;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_nota_credito_compra")
    private Integer idNotaCreditoCompra;
    @Column(name = "id_factura_compra")
    private Integer idFacturaCompra;
    @JoinColumn(name = "id_factura_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private FacturaCompra facturaCompra;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = NotaCreditoCompra.class)
    @JoinColumn(name = "id_nota_credito_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NotaCreditoCompra notaCreditoCompra;

    public NotaCreditoCompraDetalle() {
    }

    public NotaCreditoCompraDetalle(Integer id) {
        this.id = id;
    }

    public NotaCreditoCompraDetalle(Integer id, Integer nroLinea, String descripcion, Integer porcentajeIva, Integer cantidad, BigDecimal precioUnitarioConIva, BigDecimal precioUnitarioSinIva, BigDecimal montoIva, BigDecimal montoImponible, BigDecimal montoTotal) {
        this.id = id;
        this.nroLinea = nroLinea;
        this.descripcion = descripcion;
        this.porcentajeIva = porcentajeIva;
        this.cantidad = cantidad;
        this.precioUnitarioConIva = precioUnitarioConIva;
        this.precioUnitarioSinIva = precioUnitarioSinIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setIdNotaCreditoCompra(Integer idNotaCreditoCompra) {
        this.idNotaCreditoCompra = idNotaCreditoCompra;
    }

    public Integer getIdNotaCreditoCompra() {
        return idNotaCreditoCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public FacturaCompra getFacturaCompra() {
        return facturaCompra;
    }

    public void setFacturaCompra(FacturaCompra facturaCompra) {
        this.facturaCompra = facturaCompra;
    }

    public void setNotaCreditoCompra(NotaCreditoCompra notaCreditoCompra) {
        this.notaCreditoCompra = notaCreditoCompra;
    }

    public NotaCreditoCompra getNotaCreditoCompra() {
        return notaCreditoCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaCreditoCompraDetalle)) {
            return false;
        }
        NotaCreditoCompraDetalle other = (NotaCreditoCompraDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompraDetalle[ id=" + id + " ]";
    }
    
}
