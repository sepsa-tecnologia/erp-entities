/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.util.List;
import py.com.sepsa.utils.rest.parameters.AbstractParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class MotivoEmisionNc extends AbstractParam{
    
    private Integer idTipoEmision;

    public Integer getIdTipoEmision() {
        return idTipoEmision;
    }

    public void setIdTipoEmision(Integer idTipoEmision) {
        this.idTipoEmision = idTipoEmision;
    }

    public MotivoEmisionNc() {
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isValidToCreate() {
        limpiarErrores();
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isValidToEdit() {
        limpiarErrores();
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isValidToGetById() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isValidToGetByCode() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
