/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import py.com.sepsa.erp.ejb.entities.info.Aprobacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Estado;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "descuento", catalog = "erp", schema = "comercial")
@XmlRootElement
public class Descuento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_servicio")
    private Integer idServicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_descuento")
    private Integer idTipoDescuento;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_desde")
    @Temporal(TemporalType.DATE)
    private Date fechaDesde;
    @Column(name = "fecha_hasta")
    @Temporal(TemporalType.DATE)
    private Date fechaHasta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_descuento")
    private BigDecimal valorDescuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_aprobacion")
    private Integer idAprobacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "excedente")
    private Character excedente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentual")
    private Character porcentual;
    @Column(name = "id_cadena")
    private Integer idCadena;
    @Column(name = "id_contrato")
    private Integer idContrato;
    @Column(name = "id_motivo_descuento")
    private Integer idMotivoDescuento;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Contrato contrato;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_tipo_descuento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDescuento tipoDescuento;
    @JoinColumn(name = "id_cadena", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne
    private Cliente cadena;
    @JoinColumn(name = "id_motivo_descuento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MotivoDescuento motivoDescuento;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Servicio servicio;
    @JoinColumn(name = "id_aprobacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Aprobacion aprobacion;

    public Descuento() {
    }

    public Descuento(Integer id) {
        this.id = id;
    }

    public Descuento(Integer id, int idTipoDescuento, Date fechaInsercion, Date fechaDesde, Date fechaHasta, BigDecimal valorDescuento, int idEstado, Character excedente, Character porcentual, int idAprobacion) {
        this.id = id;
        this.idTipoDescuento = idTipoDescuento;
        this.fechaInsercion = fechaInsercion;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.valorDescuento = valorDescuento;
        this.idEstado = idEstado;
        this.excedente = excedente;
        this.porcentual = porcentual;
        this.idAprobacion = idAprobacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public BigDecimal getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(BigDecimal valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Character getExcedente() {
        return excedente;
    }

    public void setExcedente(Character excedente) {
        this.excedente = excedente;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Integer getIdCadena() {
        return idCadena;
    }

    public void setIdCadena(Integer idCadena) {
        this.idCadena = idCadena;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdMotivoDescuento() {
        return idMotivoDescuento;
    }

    public void setIdMotivoDescuento(Integer idMotivoDescuento) {
        this.idMotivoDescuento = idMotivoDescuento;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setAprobacion(Aprobacion aprobacion) {
        this.aprobacion = aprobacion;
    }

    public Aprobacion getAprobacion() {
        return aprobacion;
    }

    public void setIdAprobacion(Integer idAprobacion) {
        this.idAprobacion = idAprobacion;
    }

    public Integer getIdAprobacion() {
        return idAprobacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Cliente getCadena() {
        return cadena;
    }

    public void setCadena(Cliente cadena) {
        this.cadena = cadena;
    }

    public MotivoDescuento getMotivoDescuento() {
        return motivoDescuento;
    }

    public void setMotivoDescuento(MotivoDescuento motivoDescuento) {
        this.motivoDescuento = motivoDescuento;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public void setTipoDescuento(TipoDescuento tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public TipoDescuento getTipoDescuento() {
        return tipoDescuento;
    }

    public void setIdTipoDescuento(Integer idTipoDescuento) {
        this.idTipoDescuento = idTipoDescuento;
    }

    public Integer getIdTipoDescuento() {
        return idTipoDescuento;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Descuento)) {
            return false;
        }
        Descuento other = (Descuento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.comercial.Descuento[ id=" + id + " ]";
    }
    
}
