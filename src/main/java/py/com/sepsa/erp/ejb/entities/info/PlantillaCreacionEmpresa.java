/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoEmisionNc;
import py.com.sepsa.erp.ejb.entities.info.pojos.Perfil;
import py.com.sepsa.erp.ejb.entities.info.pojos.Menu;
import py.com.sepsa.erp.ejb.entities.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.ejb.entities.info.pojos.Usuario;
import py.com.sepsa.erp.ejb.hibernate.types.JsonType;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "plantilla_creacion_empresa", catalog = "erp", schema = "info")
@XmlRootElement
public class PlantillaCreacionEmpresa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @Column(name = "menu")
    @Type(type = "py.com.sepsa.erp.ejb.hibernate.types.JsonType", parameters = {
        @Parameter(
                name = JsonType.RETURNED_CLASS,
                value = "py.com.sepsa.erp.ejb.entities.info.pojos.Menu")
     })
    private List<Menu> menu; 
    
    @Column(name = "configuracion_valor")
    @Type(type = "py.com.sepsa.erp.ejb.hibernate.types.JsonType", parameters = {
        @Parameter(
                name = JsonType.RETURNED_CLASS,
                value = "py.com.sepsa.erp.ejb.entities.info.pojos.ConfiguracionValor")
     })
    private List<ConfiguracionValor> configuracionValor;  
    
    @Column(name = "perfil")
    @Type(type = "py.com.sepsa.erp.ejb.hibernate.types.JsonType", parameters = {
        @Parameter(
                name = JsonType.RETURNED_CLASS,
                value = "py.com.sepsa.erp.ejb.entities.info.pojos.Perfil")
     })
    private List<Perfil> perfil;     
    
    @Column(name = "motivo_emision_nc")
    @Type(type = "py.com.sepsa.erp.ejb.hibernate.types.JsonType", parameters = {
        @Parameter(
                name = JsonType.RETURNED_CLASS,
                value = "py.com.sepsa.erp.ejb.entities.info.pojos.MotivoEmisionNc")
     })
    private List<MotivoEmisionNc> motivoEmisionNc;
    
    @Column(name = "usuario")
    @Type(type = "py.com.sepsa.erp.ejb.hibernate.types.JsonType", parameters = {
        @Parameter(
                name = JsonType.RETURNED_CLASS,
                value = "py.com.sepsa.erp.ejb.entities.info.pojos.Usuario")
     })
    private List<Usuario> usuario;
    
        
     public PlantillaCreacionEmpresa() {
    }

    public PlantillaCreacionEmpresa(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }
   
    public List<Menu> getMenu() {
        return menu;
    }

    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

    public List<ConfiguracionValor> getConfiguracionValor() {
        return configuracionValor;
    }

    public void setConfiguracionValor(List<ConfiguracionValor> configuracionValor) {
        this.configuracionValor = configuracionValor;
    }

    public List<Perfil> getPerfil() {
        return perfil;
    }

    public void setPerfil(List<Perfil> perfil) {
        this.perfil = perfil;
    }

    public List<MotivoEmisionNc> getMotivoEmisionNc() {
        return motivoEmisionNc;
    }

    public void setMotivoEmisionNc(List<MotivoEmisionNc> motivoEmisionNc) {
        this.motivoEmisionNc = motivoEmisionNc;
    }

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }
    
      @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
   
        PlantillaCreacionEmpresa other = (PlantillaCreacionEmpresa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.PlantillaCreacionEmpresa[ id=" + id + " ]";
    }
    

}
