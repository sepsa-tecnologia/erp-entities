/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.hibernate.types;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
import py.com.sepsa.utils.gson.GsonUtils;


/**
 *
 * @author Jonathan Bernal
 */
public class JsonType implements UserType, ParameterizedType {

    public static final String RETURNED_CLASS = "RETURNED_CLASS";
    private static final Gson GSON = GsonUtils.generateDetault();

    private Class<? extends Object> returnedClass;

    @Override
    public int[] sqlTypes() {
        return new int[]{1111};
    }

    @Override
    public Class<? extends Object> returnedClass() {
        return returnedClass;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
            throws HibernateException, SQLException {
        String value = rs.getString(names[0]);
        if (value == null) {
            return null;
        } else {
            try{
                Object obj = GSON.fromJson(value, returnedClass);
            } catch (JsonSyntaxException e){
                this.returnedClass.getClass();
               // Type listType = new TypeToken<List<? extends Object>>() {}.getType();
               // List<? extends Object> lista = GSON.fromJson(value, listType);
               
               java.lang.reflect.Type listType = TypeToken.getParameterized(List.class, returnedClass).getType();
                return GSON.fromJson(value, listType);

            }

            return GSON.fromJson(value, returnedClass);
        }
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        if (value != null) {
            st.setObject(index, GSON.toJsonTree(value, returnedClass).toString(), sqlTypes()[0]);
        } else {
            st.setNull(index, sqlTypes()[0]);
        }
    }

    @Override
    public boolean equals(Object o, Object o1) throws HibernateException {
        return Objects.equals(o, o1);
    }

    @Override
    public int hashCode(Object o) throws HibernateException {
        return Objects.hashCode(o);
    }

    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return o;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        Object deepCopy = deepCopy(0);
        if (!(deepCopy instanceof Serializable)) {
            return (Serializable) deepCopy;
        }
        return null;
    }

    @Override
    public Object assemble(Serializable srlzbl, Object o) throws HibernateException {
        return deepCopy(srlzbl);
    }

    @Override
    public Object replace(Object o, Object o1, Object o2) throws HibernateException {
        return deepCopy(o);
    }

    @Override
    public void setParameterValues(Properties properties) {
        String clazz = (String) properties.get(RETURNED_CLASS);
        try {
            returnedClass = ReflectHelper.classForName(getClazzName(clazz));
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Class: " + clazz + " is not a known class type.");
        }
    }

    private String getClazzName(String clazz) {
        if (clazz == null) {
            return Object.class.getName();
        }

        switch (clazz) {
            case "LIST":
                return List.class.getName();
            case "JSON":
                return JsonElement.class.getName();
            default:
                return clazz;
        }
    }
}
