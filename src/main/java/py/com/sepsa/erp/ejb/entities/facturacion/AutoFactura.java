/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "autofactura", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class AutoFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_naturaleza_vendedor")
    private Integer idNaturalezaVendedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_autofactura")
    private String nroAutofactura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social")
    private String razonSocial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Size(max = 2147483647)
    @Column(name = "nro_documento_vendedor")
    private String nroDocumentoVendedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_vendedor")
    private String nombreVendedor;
    @Size(max = 2147483647)
    @Column(name = "ruc")
    private String ruc;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "digital")
    private Character digital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cobrado")
    private Character cobrado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "impreso")
    private Character impreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entregado")
    private Character entregado;
    @Column(name = "generado")
    private Character generado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "archivo_edi")
    private Character archivoEdi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "archivo_set")
    private Character archivoSet;
    @Column(name = "generado_edi")
    private Character generadoEdi;
    @Column(name = "generado_set")
    private Character generadoSet;
    @Column(name = "estado_sincronizado")
    private Character estadoSincronizado;
    @Column(name = "fecha_entrega")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrega;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_descuento_particular")
    private BigDecimal montoTotalDescuentoParticular;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_descuento_global")
    private BigDecimal montoTotalDescuentoGlobal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_autofactura")
    private BigDecimal montoTotalAutofactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_guaranies")
    private BigDecimal montoTotalGuaranies;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldo")
    private BigDecimal saldo;
    @Size(max = 2147483647)
    @Column(name = "cdc")
    private String cdc;
    @Column(name = "cod_seguridad")
    private String codSeguridad;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "nro_casa")
    private Integer nroCasa;
    @Column(name = "id_departamento")
    private Integer idDepartamento;
    @Column(name = "id_distrito")
    private Integer idDistrito;
    @Column(name = "id_ciudad")
    private Integer idCiudad;
    @Column(name = "id_departamento_transaccion")
    private Integer idDepartamentoTransaccion;
    @Column(name = "id_distrito_transaccion")
    private Integer idDistritoTransaccion;
    @Column(name = "id_ciudad_transaccion")
    private Integer idCiudadTransaccion;
    @Column(name = "id_tipo_cambio")
    private Integer idTipoCambio;
    @NotNull
    @Column(name = "id_talonario")
    private Integer idTalonario;
    @NotNull
    @Column(name = "id_tipo_factura")
    private Integer idTipoFactura;
    @Column(name = "id_encargado")
    private Integer idEncargado;
    @Size(max = 2147483647)
    @Column(name = "serie")
    private String serie;
    @Column(name = "id_motivo_anulacion")
    private Integer idMotivoAnulacion;
    @Column(name = "observacion_anulacion")
    private String observacionAnulacion;
    @Column(name = "id_local_origen")
    private Integer idLocalOrigen;
    @Column(name = "id_local_destino")
    private Integer idLocalDestino;
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_procesamiento")
    private Integer idProcesamiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_constancia")
    private String nroConstancia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_control_constancia")
    private String nroControlConstancia;
    @JoinColumn(name = "id_tipo_cambio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private TipoCambio tipoCambio;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Moneda moneda;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Talonario talonario;
    @JoinColumn(name = "id_tipo_factura", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private TipoFactura tipoFactura;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_encargado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Usuario encargado;
    @JoinColumn(name = "id_motivo_anulacion", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private MotivoAnulacion motivoAnulacion;
    @JoinColumn(name = "id_local_origen", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localOrigen;
    @JoinColumn(name = "id_local_destino", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localDestino;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Estado estado;
    @JoinColumn(name = "id_procesamiento", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Procesamiento procesamiento;
    @JoinColumn(name = "id_naturaleza_vendedor", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private NaturalezaVendedor naturalezaVendedor;
    @GsonRepellent
    @JoinColumn(name = "id", referencedColumnName = "id_factura", updatable = false, insertable = false)
    @OneToOne
    private FacturaDncp facturaDncp;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = AutoFacturaDetalle.class)
    @OneToMany(mappedBy = "autoFactura")
    private Collection<AutoFacturaDetalle> facturaDetalles;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = FacturaDescuento.class)
    @OneToMany(mappedBy = "factura")
    private Collection<FacturaDescuento> facturaDescuentos;

    public AutoFactura() {
    }

    public AutoFactura(Integer id) {
        this.id = id;
    }

    public AutoFactura(Integer id, int idMoneda, String nroFactura, Date fecha, String nombreVendedor, Character digital, Character anulado, 
            Character cobrado, Character impreso, Character entregado, BigDecimal montoTotalAutofactura ,BigDecimal montoTotalGuaranies) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.nroAutofactura = nroFactura;
        this.fecha = fecha;
        this.nombreVendedor = nombreVendedor;
        this.digital = digital;
        this.anulado = anulado;
        this.cobrado = cobrado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.montoTotalAutofactura = montoTotalAutofactura;
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getIdMoneda() {
        return idMoneda;
    }


    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroAutofactura() {
        return nroAutofactura;
    }

    public void setNroAutofactura(String nroAutofactura) {
        this.nroAutofactura = nroAutofactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getNroDocumentoVendedor() {
        return nroDocumentoVendedor;
    }

    public void setNroDocumentoVendedor(String nroDocumentoVendedor) {
        this.nroDocumentoVendedor = nroDocumentoVendedor;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getCobrado() {
        return cobrado;
    }

    public void setCobrado(Character cobrado) {
        this.cobrado = cobrado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }


    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

   

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public TipoFactura getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(TipoFactura tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public void setMontoTotalDescuentoParticular(BigDecimal montoTotalDescuentoParticular) {
        this.montoTotalDescuentoParticular = montoTotalDescuentoParticular;
    }

    public BigDecimal getMontoTotalDescuentoParticular() {
        return montoTotalDescuentoParticular;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setFacturaDescuentos(Collection<FacturaDescuento> facturaDescuentos) {
        this.facturaDescuentos = facturaDescuentos;
    }

    public Collection<FacturaDescuento> getFacturaDescuentos() {
        return facturaDescuentos;
    }

//    public Collection<AutoFacturaDetalle> getFacturaDetalles() {
//        return facturaDetalles;
//    }
//
//    public void setFacturaDetalles(Collection<AutoFacturaDetalle> facturaDetalles) {
//        this.facturaDetalles = facturaDetalles;
//    }

    public void setProcesamiento(Procesamiento procesamiento) {
        this.procesamiento = procesamiento;
    }

    public Procesamiento getProcesamiento() {
        return procesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setFacturaDncp(FacturaDncp facturaDncp) {
        this.facturaDncp = facturaDncp;
    }

    public FacturaDncp getFacturaDncp() {
        return facturaDncp;
    }

    public Integer getIdNaturalezaVendedor() {
        return idNaturalezaVendedor;
    }

    public void setIdNaturalezaVendedor(Integer idNaturalezaVendedor) {
        this.idNaturalezaVendedor = idNaturalezaVendedor;
    }

    public Character getGenerado() {
        return generado;
    }

    public void setGenerado(Character generado) {
        this.generado = generado;
    }

    public Integer getIdDepartamentoTransaccion() {
        return idDepartamentoTransaccion;
    }

    public void setIdDepartamentoTransaccion(Integer idDepartamentoTransaccion) {
        this.idDepartamentoTransaccion = idDepartamentoTransaccion;
    }

    public Integer getIdDistritoTransaccion() {
        return idDistritoTransaccion;
    }

    public void setIdDistritoTransaccion(Integer idDistritoTransaccion) {
        this.idDistritoTransaccion = idDistritoTransaccion;
    }

    public Integer getIdCiudadTransaccion() {
        return idCiudadTransaccion;
    }

    public void setIdCiudadTransaccion(Integer idCiudadTransaccion) {
        this.idCiudadTransaccion = idCiudadTransaccion;
    }

    public NaturalezaVendedor getNaturalezaVendedor() {
        return naturalezaVendedor;
    }

    public void setNaturalezaVendedor(NaturalezaVendedor naturalezaVendedor) {
        this.naturalezaVendedor = naturalezaVendedor;
    }

    public Collection<AutoFacturaDetalle> getFacturaDetalles() {
        return facturaDetalles;
    }

    public void setFacturaDetalles(Collection<AutoFacturaDetalle> facturaDetalles) {
        this.facturaDetalles = facturaDetalles;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public BigDecimal getMontoTotalAutofactura() {
        return montoTotalAutofactura;
    }

    public void setMontoTotalAutofactura(BigDecimal montoTotalAutofactura) {
        this.montoTotalAutofactura = montoTotalAutofactura;
    }

    public String getNroConstancia() {
        return nroConstancia;
    }

    public void setNroConstancia(String nroConstancia) {
        this.nroConstancia = nroConstancia;
    }

    public String getNroControlConstancia() {
        return nroControlConstancia;
    }

    public void setNroControlConstancia(String nroControlConstancia) {
        this.nroControlConstancia = nroControlConstancia;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutoFactura)) {
            return false;
        }
        AutoFactura other = (AutoFactura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.Factura[ id=" + id + " ]";
    }

}
