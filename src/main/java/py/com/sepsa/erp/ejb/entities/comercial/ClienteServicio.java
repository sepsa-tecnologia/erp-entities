/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "cliente_servicio", catalog = "erp", schema = "comercial")
@XmlRootElement
public class ClienteServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ClienteServicioPK clienteServicioPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private int idEstado;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicio servicio;

    public ClienteServicio() {
    }

    public ClienteServicio(ClienteServicioPK clienteServicioPK) {
        this.clienteServicioPK = clienteServicioPK;
    }

    public ClienteServicio(ClienteServicioPK clienteServicioPK, int idEstado) {
        this.clienteServicioPK = clienteServicioPK;
        this.idEstado = idEstado;
    }

    public ClienteServicio(int idCliente, int idServicio) {
        this.clienteServicioPK = new ClienteServicioPK(idCliente, idServicio);
    }

    public ClienteServicioPK getClienteServicioPK() {
        return clienteServicioPK;
    }

    public void setClienteServicioPK(ClienteServicioPK clienteServicioPK) {
        this.clienteServicioPK = clienteServicioPK;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clienteServicioPK != null ? clienteServicioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteServicio)) {
            return false;
        }
        ClienteServicio other = (ClienteServicio) object;
        if ((this.clienteServicioPK == null && other.clienteServicioPK != null) || (this.clienteServicioPK != null && !this.clienteServicioPK.equals(other.clienteServicioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.ClienteServicio[ clienteServicioPK=" + clienteServicioPK + " ]";
    }
    
}
