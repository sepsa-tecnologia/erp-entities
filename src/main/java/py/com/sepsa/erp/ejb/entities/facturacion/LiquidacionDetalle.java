/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "liquidacion_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class LiquidacionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_historico_liquidacion")
    private Integer idHistoricoLiquidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Column(name = "id_monto_tarifa")
    private Integer idMontoTarifa;
    @Column(name = "id_descuento")
    private Integer idDescuento;
    @Column(name = "id_cliente_rel")
    private Integer idClienteRel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_tarifa")
    private BigDecimal montoTarifa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento")
    private BigDecimal montoDescuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @JoinColumn(name = "id_historico_liquidacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private HistoricoLiquidacion historicoLiquidacion;

    public Integer getIdMontoTarifa() {
        return idMontoTarifa;
    }

    public void setIdMontoTarifa(Integer idMontoTarifa) {
        this.idMontoTarifa = idMontoTarifa;
    }

    public Integer getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(Integer idDescuento) {
        this.idDescuento = idDescuento;
    }

    public Integer getIdClienteRel() {
        return idClienteRel;
    }

    public void setIdClienteRel(Integer idClienteRel) {
        this.idClienteRel = idClienteRel;
    }

    public BigDecimal getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(BigDecimal montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public BigDecimal getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(BigDecimal montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public HistoricoLiquidacion getHistoricoLiquidacion() {
        return historicoLiquidacion;
    }

    public void setHistoricoLiquidacion(HistoricoLiquidacion historicoLiquidacion) {
        this.historicoLiquidacion = historicoLiquidacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionDetalle)) {
            return false;
        }
        LiquidacionDetalle other = (LiquidacionDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.LiquidacionDetalle[ id=" + id + " ]";
    }
    
}
