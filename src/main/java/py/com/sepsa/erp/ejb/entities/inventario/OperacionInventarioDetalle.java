/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario;

import py.com.sepsa.erp.ejb.entities.info.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "operacion_inventario_detalle", catalog = "erp", schema = "inventario")
@XmlRootElement
public class OperacionInventarioDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_operacion_inventario")
    private Integer idOperacionInventario;
    @Column(name = "id_estado_inventario_origen")
    private Integer idEstadoInventarioOrigen;
    @Column(name = "id_estado_inventario_destino")
    private Integer idEstadoInventarioDestino;
    @NotNull
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "id_deposito_origen")
    private Integer idDepositoOrigen;
    @Column(name = "id_deposito_destino")
    private Integer idDepositoDestino;
    @NotNull
    @Column(name = "cantidad")
    private Integer cantidad;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = OperacionInventario.class)
    @JoinColumn(name = "id_operacion_inventario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private OperacionInventario operacionInventario;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "id_estado_inventario_origen", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Estado estadoInventarioOrigen;
    @JoinColumn(name = "id_estado_inventario_destino", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estadoInventarioDestino;
    @JoinColumn(name = "id_deposito_origen", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private DepositoLogistico depositoOrigen;
    @JoinColumn(name = "id_deposito_destino", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private DepositoLogistico depositoDestino;

    public OperacionInventarioDetalle() {
    }

    public OperacionInventarioDetalle(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdEstadoInventarioOrigen(Integer idEstadoInventarioOrigen) {
        this.idEstadoInventarioOrigen = idEstadoInventarioOrigen;
    }

    public Integer getIdEstadoInventarioOrigen() {
        return idEstadoInventarioOrigen;
    }

    public void setIdEstadoInventarioDestino(Integer idEstadoInventarioDestino) {
        this.idEstadoInventarioDestino = idEstadoInventarioDestino;
    }

    public Integer getIdEstadoInventarioDestino() {
        return idEstadoInventarioDestino;
    }

    public void setEstadoInventarioOrigen(Estado estadoInventarioOrigen) {
        this.estadoInventarioOrigen = estadoInventarioOrigen;
    }

    public Estado getEstadoInventarioOrigen() {
        return estadoInventarioOrigen;
    }

    public void setEstadoInventarioDestino(Estado estadoInventarioDestino) {
        this.estadoInventarioDestino = estadoInventarioDestino;
    }

    public Estado getEstadoInventarioDestino() {
        return estadoInventarioDestino;
    }

    public void setOperacionInventario(OperacionInventario operacionInventario) {
        this.operacionInventario = operacionInventario;
    }

    public OperacionInventario getOperacionInventario() {
        return operacionInventario;
    }

    public void setIdOperacionInventario(Integer idOperacionInventario) {
        this.idOperacionInventario = idOperacionInventario;
    }

    public Integer getIdOperacionInventario() {
        return idOperacionInventario;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setIdDepositoOrigen(Integer idDepositoOrigen) {
        this.idDepositoOrigen = idDepositoOrigen;
    }

    public Integer getIdDepositoOrigen() {
        return idDepositoOrigen;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setDepositoOrigen(DepositoLogistico depositoOrigen) {
        this.depositoOrigen = depositoOrigen;
    }

    public DepositoLogistico getDepositoOrigen() {
        return depositoOrigen;
    }

    public void setDepositoDestino(DepositoLogistico depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

    public DepositoLogistico getDepositoDestino() {
        return depositoDestino;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OperacionInventarioDetalle)) {
            return false;
        }
        OperacionInventarioDetalle other = (OperacionInventarioDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.inventario.OperacionInventarioDetalle[ id=" + id + " ]";
    }
    
}
