/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "persona_etiqueta", catalog = "erp", schema = "info")
@XmlRootElement
public class PersonaEtiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PersonaEtiquetaPK personaEtiquetaPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_etiqueta", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Etiqueta etiqueta;
    @JoinColumn(name = "id_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;

    public PersonaEtiqueta() {
    }

    public PersonaEtiqueta(PersonaEtiquetaPK personaEtiquetaPK) {
        this.personaEtiquetaPK = personaEtiquetaPK;
    }

    public PersonaEtiqueta(PersonaEtiquetaPK personaEtiquetaPK, Character activo) {
        this.personaEtiquetaPK = personaEtiquetaPK;
        this.activo = activo;
    }

    public PersonaEtiqueta(int idPersona, int idEtiqueta) {
        this.personaEtiquetaPK = new PersonaEtiquetaPK(idPersona, idEtiqueta);
    }

    public PersonaEtiquetaPK getPersonaEtiquetaPK() {
        return personaEtiquetaPK;
    }

    public void setPersonaEtiquetaPK(PersonaEtiquetaPK personaEtiquetaPK) {
        this.personaEtiquetaPK = personaEtiquetaPK;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Etiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personaEtiquetaPK != null ? personaEtiquetaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaEtiqueta)) {
            return false;
        }
        PersonaEtiqueta other = (PersonaEtiqueta) object;
        if ((this.personaEtiquetaPK == null && other.personaEtiquetaPK != null) || (this.personaEtiquetaPK != null && !this.personaEtiquetaPK.equals(other.personaEtiquetaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.PersonaEtiqueta[ personaEtiquetaPK=" + personaEtiquetaPK + " ]";
    }
    
}
