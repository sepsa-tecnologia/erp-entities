/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "ejecucion_error", catalog = "erp", schema = "proceso")
@XmlRootElement
public class EjecucionError implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Size(max = 2147483647)
    @Column(name = "dato")
    private String dato;
    @NotNull
    @Column(name = "id_ejecucion_proceso")
    private Integer idEjecucionProceso;
    @NotNull
    @Column(name = "id_error_proceso")
    private Integer idErrorProceso;
    @JoinColumn(name = "id_ejecucion_proceso", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EjecucionProceso ejecucionProceso;
    @JoinColumn(name = "id_error_proceso", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ErrorProceso errorProceso;

    public EjecucionError() {
    }

    public EjecucionError(Integer id) {
        this.id = id;
    }

    public EjecucionError(Integer id, Date fechaInsercion) {
        this.id = id;
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public void setIdErrorProceso(Integer idErrorProceso) {
        this.idErrorProceso = idErrorProceso;
    }

    public Integer getIdErrorProceso() {
        return idErrorProceso;
    }

    public void setIdEjecucionProceso(Integer idEjecucionProceso) {
        this.idEjecucionProceso = idEjecucionProceso;
    }

    public Integer getIdEjecucionProceso() {
        return idEjecucionProceso;
    }

    public void setErrorProceso(ErrorProceso errorProceso) {
        this.errorProceso = errorProceso;
    }

    public ErrorProceso getErrorProceso() {
        return errorProceso;
    }

    public void setEjecucionProceso(EjecucionProceso ejecucionProceso) {
        this.ejecucionProceso = ejecucionProceso;
    }

    public EjecucionProceso getEjecucionProceso() {
        return ejecucionProceso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EjecucionError)) {
            return false;
        }
        EjecucionError other = (EjecucionError) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.proceso.EjecucionError[ id=" + id + " ]";
    }
    
}
