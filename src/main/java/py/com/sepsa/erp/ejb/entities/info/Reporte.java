/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import com.google.gson.JsonElement;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import py.com.sepsa.erp.ejb.hibernate.types.JsonType;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "reporte", catalog = "erp", schema = "info")
@XmlRootElement
public class Reporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo_tipo_reporte")
    private String codigoTipoReporte;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "formato")
    private String formato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @NotNull
    @Column(name = "parametros")
    @Type(type = "py.com.sepsa.erp.ejb.hibernate.types.JsonType", parameters = {
        @Parameter(
                name = JsonType.RETURNED_CLASS,
                value = "JSON")
     })
    private JsonElement parametros;

    public Reporte() {
    }

    public Reporte(Integer id) {
        this.id = id;
    }

    public Reporte(Integer id, String descripcion, String codigo, JsonElement parametros) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.parametros = parametros;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getFormato() {
        return formato;
    }

    public void setParametros(JsonElement parametros) {
        this.parametros = parametros;
    }

    public JsonElement getParametros() {
        return parametros;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public void setCodigoTipoReporte(String codigoTipoReporte) {
        this.codigoTipoReporte = codigoTipoReporte;
    }

    public String getCodigoTipoReporte() {
        return codigoTipoReporte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reporte)) {
            return false;
        }
        Reporte other = (Reporte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.Estado[ id=" + id + " ]";
    }
    
}
