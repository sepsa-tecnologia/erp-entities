/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "dato_persona", catalog = "erp", schema = "info")
@XmlRootElement
public class DatoPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DatoPersonaPK datoPersonaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "valor")
    private String valor;
    @JoinColumn(name = "id_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;
    @JoinColumn(name = "id_tipo_dato_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDatoPersona tipoDatoPersona;

    public DatoPersona() {
    }

    public DatoPersona(DatoPersonaPK datoPersonaPK) {
        this.datoPersonaPK = datoPersonaPK;
    }

    public DatoPersona(DatoPersonaPK datoPersonaPK, String valor) {
        this.datoPersonaPK = datoPersonaPK;
        this.valor = valor;
    }

    public DatoPersona(int idPersona, int idTipoDatoPersona) {
        this.datoPersonaPK = new DatoPersonaPK(idPersona, idTipoDatoPersona);
    }

    public DatoPersonaPK getDatoPersonaPK() {
        return datoPersonaPK;
    }

    public void setDatoPersonaPK(DatoPersonaPK datoPersonaPK) {
        this.datoPersonaPK = datoPersonaPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public TipoDatoPersona getTipoDatoPersona() {
        return tipoDatoPersona;
    }

    public void setTipoDatoPersona(TipoDatoPersona tipoDatoPersona) {
        this.tipoDatoPersona = tipoDatoPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (datoPersonaPK != null ? datoPersonaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoPersona)) {
            return false;
        }
        DatoPersona other = (DatoPersona) object;
        if ((this.datoPersonaPK == null && other.datoPersonaPK != null) || (this.datoPersonaPK != null && !this.datoPersonaPK.equals(other.datoPersonaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.DatoPersona[ datoPersonaPK=" + datoPersonaPK + " ]";
    }
    
}
