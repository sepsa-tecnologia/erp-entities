/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Etiqueta;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "perfil_etiqueta", catalog = "erp", schema = "usuario")
@XmlRootElement
public class PerfilEtiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_perfil")
    private Integer idPerfil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_etiqueta")
    private Integer idEtiqueta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_perfil", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Perfil perfil;
    @JoinColumn(name = "id_etiqueta", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Etiqueta etiqueta;

    public PerfilEtiqueta() {
    }

    public PerfilEtiqueta(Integer id) {
        this.id = id;
    }

    public PerfilEtiqueta(Integer id, Integer idPerfil, Integer idEtiqueta, Character activo) {
        this.id = id;
        this.idPerfil = idPerfil;
        this.idEtiqueta = idEtiqueta;
        this.activo = activo;
    }

    public PerfilEtiqueta(Integer idPerfil, Integer idEtiqueta, Character activo) {
        this.idPerfil = idPerfil;
        this.idEtiqueta = idEtiqueta;
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public Integer getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(Integer idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    public Etiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioLocal)) {
            return false;
        }
        PerfilEtiqueta other = (PerfilEtiqueta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.usuario.PerfilEtiqueta[ id=" + id + " ]";
    }

}
