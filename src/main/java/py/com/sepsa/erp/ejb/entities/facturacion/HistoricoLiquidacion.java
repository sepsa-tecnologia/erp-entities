/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "historico_liquidacion", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class HistoricoLiquidacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_proceso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProceso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_tarifa_detalle")
    private BigDecimal montoTarifaDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_detalle")
    private BigDecimal montoDescuentoDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_detalle")
    private BigDecimal montoTotalDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_excedente")
    private BigDecimal montoTotalExcedente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_general")
    private BigDecimal montoDescuentoGeneral;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_excedente")
    private BigDecimal montoDescuentoExcedente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_parcial")
    private BigDecimal montoTotalParcial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_liquidado")
    private BigDecimal montoTotalLiquidado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_acuerdo")
    private Character montoAcuerdo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_minimo")
    private Character montoMinimo;
    @Column(name = "id_monto_minimo")
    private Integer idMontoMinimo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cobro_adelantado")
    private Character cobroAdelantado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "bonificado")
    private Character bonificado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_liquidacion")
    private Integer idLiquidacion;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "id_liquidacion", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Liquidacion liquidacion;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;

    public HistoricoLiquidacion() {
    }

    public HistoricoLiquidacion(Integer id) {
        this.id = id;
    }

    public HistoricoLiquidacion(Integer id, Date fechaProceso, BigDecimal montoTotalDetalle, BigDecimal montoTotalExcedente, BigDecimal montoDescuentoGeneral, BigDecimal montoDescuentoExcedente, BigDecimal montoDescuentoDetalle, BigDecimal montoTotalLiquidado, Character montoMinimo, Character cobroAdelantado, int idLiquidacion) {
        this.id = id;
        this.fechaProceso = fechaProceso;
        this.montoTotalDetalle = montoTotalDetalle;
        this.montoTotalExcedente = montoTotalExcedente;
        this.montoDescuentoGeneral = montoDescuentoGeneral;
        this.montoDescuentoExcedente = montoDescuentoExcedente;
        this.montoDescuentoDetalle = montoDescuentoDetalle;
        this.montoTotalLiquidado = montoTotalLiquidado;
        this.montoMinimo = montoMinimo;
        this.cobroAdelantado = cobroAdelantado;
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public BigDecimal getMontoTotalDetalle() {
        return montoTotalDetalle;
    }

    public void setMontoTotalDetalle(BigDecimal montoTotalDetalle) {
        this.montoTotalDetalle = montoTotalDetalle;
    }

    public BigDecimal getMontoTotalExcedente() {
        return montoTotalExcedente;
    }

    public void setMontoTotalExcedente(BigDecimal montoTotalExcedente) {
        this.montoTotalExcedente = montoTotalExcedente;
    }

    public BigDecimal getMontoDescuentoGeneral() {
        return montoDescuentoGeneral;
    }

    public void setMontoDescuentoGeneral(BigDecimal montoDescuentoGeneral) {
        this.montoDescuentoGeneral = montoDescuentoGeneral;
    }

    public BigDecimal getMontoDescuentoExcedente() {
        return montoDescuentoExcedente;
    }

    public void setMontoDescuentoExcedente(BigDecimal montoDescuentoExcedente) {
        this.montoDescuentoExcedente = montoDescuentoExcedente;
    }

    public BigDecimal getMontoDescuentoDetalle() {
        return montoDescuentoDetalle;
    }

    public void setMontoDescuentoDetalle(BigDecimal montoDescuentoDetalle) {
        this.montoDescuentoDetalle = montoDescuentoDetalle;
    }

    public BigDecimal getMontoTotalLiquidado() {
        return montoTotalLiquidado;
    }

    public void setMontoTotalLiquidado(BigDecimal montoTotalLiquidado) {
        this.montoTotalLiquidado = montoTotalLiquidado;
    }

    public Character getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(Character montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public Character getBonificado() {
        return bonificado;
    }

    public void setBonificado(Character bonificado) {
        this.bonificado = bonificado;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getMontoTarifaDetalle() {
        return montoTarifaDetalle;
    }

    public void setMontoTarifaDetalle(BigDecimal montoTarifaDetalle) {
        this.montoTarifaDetalle = montoTarifaDetalle;
    }

    public BigDecimal getMontoTotalParcial() {
        return montoTotalParcial;
    }

    public void setMontoTotalParcial(BigDecimal montoTotalParcial) {
        this.montoTotalParcial = montoTotalParcial;
    }

    public Character getMontoAcuerdo() {
        return montoAcuerdo;
    }

    public void setMontoAcuerdo(Character montoAcuerdo) {
        this.montoAcuerdo = montoAcuerdo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoLiquidacion)) {
            return false;
        }
        HistoricoLiquidacion other = (HistoricoLiquidacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.HistoricoLiquidacion[ id=" + id + " ]";
    }
    
}
