/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.TipoEtiqueta;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "motivo_descuento", catalog = "erp", schema = "comercial")
@XmlRootElement
public class MotivoDescuento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rango_desde")
    private BigDecimal rangoDesde;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rango_hasta")
    private BigDecimal rangoHasta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentual")
    private Character porcentual;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_etiqueta")
    private Integer idTipoEtiqueta;
    @Column(name = "fecha_desde")
    @Temporal(TemporalType.DATE)
    private Date fechaDesde;
    @Column(name = "fecha_hasta")
    @Temporal(TemporalType.DATE)
    private Date fechaHasta;
    @JoinColumn(name = "id_tipo_etiqueta", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private TipoEtiqueta tipoEtiqueta;

    public MotivoDescuento() {
    }

    public MotivoDescuento(Integer id) {
        this.id = id;
    }

    public MotivoDescuento(Integer id, String descripcion, BigDecimal rangoDesde, BigDecimal rangoHasta, Character porcentual, Character activo, Integer idTipoEtiqueta, Date fechaDesde, Date fechaHasta) {
        this.id = id;
        this.descripcion = descripcion;
        this.rangoDesde = rangoDesde;
        this.rangoHasta = rangoHasta;
        this.porcentual = porcentual;
        this.activo = activo;
        this.idTipoEtiqueta = idTipoEtiqueta;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getRangoDesde() {
        return rangoDesde;
    }

    public void setRangoDesde(BigDecimal rangoDesde) {
        this.rangoDesde = rangoDesde;
    }

    public BigDecimal getRangoHasta() {
        return rangoHasta;
    }

    public void setRangoHasta(BigDecimal rangoHasta) {
        this.rangoHasta = rangoHasta;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Integer getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(Integer idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public TipoEtiqueta getTipoEtiqueta() {
        return tipoEtiqueta;
    }

    public void setTipoEtiqueta(TipoEtiqueta tipoEtiqueta) {
        this.tipoEtiqueta = tipoEtiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MotivoDescuento)) {
            return false;
        }
        MotivoDescuento other = (MotivoDescuento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.MotivoDescuento[ id=" + id + " ]";
    }
    
}
