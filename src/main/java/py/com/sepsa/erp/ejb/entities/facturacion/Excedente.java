/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "excedente", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class Excedente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_servicio")
    private Integer idServicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad_maxima")
    private int cantidadMaxima;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_excedente")
    private BigInteger montoExcedente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicio servicio;

    public Excedente() {
    }

    public Excedente(Integer id) {
        this.id = id;
    }

    public Excedente(Integer id, int idServicio, int cantidadMaxima, BigInteger montoExcedente, Character activo) {
        this.id = id;
        this.idServicio = idServicio;
        this.cantidadMaxima = cantidadMaxima;
        this.montoExcedente = montoExcedente;
        this.activo = activo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCantidadMaxima() {
        return cantidadMaxima;
    }

    public void setCantidadMaxima(int cantidadMaxima) {
        this.cantidadMaxima = cantidadMaxima;
    }

    public BigInteger getMontoExcedente() {
        return montoExcedente;
    }

    public void setMontoExcedente(BigInteger montoExcedente) {
        this.montoExcedente = montoExcedente;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Excedente)) {
            return false;
        }
        Excedente other = (Excedente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.Excedente[ id=" + id + " ]";
    }
    
}
