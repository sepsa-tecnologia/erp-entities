/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario;

import py.com.sepsa.erp.ejb.entities.info.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "operacion_inventario", catalog = "erp", schema = "inventario")
@XmlRootElement
public class OperacionInventario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @NotNull
    @Column(name = "id_tipo_operacion")
    private Integer idTipoOperacion;
    @NotNull
    @Column(name = "id_motivo")
    private Integer idMotivo;
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_orden_compra")
    private Integer idOrdenCompra;
    @Column(name = "id_control_inventario")
    private Integer idControlInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @JoinColumn(name = "id_tipo_operacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoOperacion tipoOperacion;
    @JoinColumn(name = "id_motivo", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Motivo motivo;
    @JoinColumn(name = "id_orden_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private OrdenCompra ordenCompra;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Usuario usuario;

    public OperacionInventario() {
    }

    public OperacionInventario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public TipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(TipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public void setOrdenCompra(OrdenCompra ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public OrdenCompra getOrdenCompra() {
        return ordenCompra;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OperacionInventario)) {
            return false;
        }
        OperacionInventario other = (OperacionInventario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.inventario.OperacionInventario[ id=" + id + " ]";
    }
    
}
