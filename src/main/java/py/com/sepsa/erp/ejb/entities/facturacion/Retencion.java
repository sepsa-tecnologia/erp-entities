/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "retencion", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class Retencion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_retencion")
    private String nroRetencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_retencion")
    private Integer porcentajeRetencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_retenido_total")
    private BigDecimal montoRetenidoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_motivo_anulacion")
    private Integer idMotivoAnulacion;
    @Column(name = "observacion_anulacion")
    private String observacionAnulacion;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", updatable = false, insertable = false)
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(name = "id_motivo_anulacion", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private MotivoAnulacion motivoAnulacion;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = RetencionDetalle.class)
    @OneToMany(mappedBy = "retencion")
    private Collection<RetencionDetalle> retencionDetalles;

    public Retencion() {
    }

    public Retencion(Integer id) {
        this.id = id;
    }

    public Retencion(Integer id, String nroRetencion, Date fecha, BigDecimal montoImponibleTotal, BigDecimal montoIvaTotal, BigDecimal montoTotal, Integer porcentajeRetencion, BigDecimal montoRetenidoTotal) {
        this.id = id;
        this.nroRetencion = nroRetencion;
        this.fecha = fecha;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoIvaTotal = montoIvaTotal;
        this.montoTotal = montoTotal;
        this.porcentajeRetencion = porcentajeRetencion;
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public void setRetencionDetalles(Collection<RetencionDetalle> retencionDetalles) {
        this.retencionDetalles = retencionDetalles;
    }

    public Collection<RetencionDetalle> getRetencionDetalles() {
        return retencionDetalles;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retencion)) {
            return false;
        }
        Retencion other = (Retencion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.Retencion[ id=" + id + " ]";
    }
    
}
