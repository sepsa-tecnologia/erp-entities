/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Email;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario", catalog = "erp", schema = "usuario")
@XmlRootElement
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "usuario")
    private String usuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "contrasena")
    private String contrasena;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "hash")
    private String hash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_email")
    private Integer idEmail;
    @Column(name = "id_telefono")
    private Integer idTelefono;
    @Column(name = "id_persona_fisica")
    private Integer idPersonaFisica;
    @JoinColumn(name = "id_persona_fisica", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Persona personaFisica;
    @JoinColumn(name = "id_email", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Email email;
    @JoinColumn(name = "id_telefono", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Telefono telefono;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = UsuarioPerfil.class)
    @OneToMany(mappedBy = "usuario")
    private Collection<UsuarioPerfil> usuarioPerfiles;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = UsuarioLocal.class)
    @OneToMany(mappedBy = "usuario")
    private Collection<UsuarioLocal> usuarioLocales;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = UsuarioEmpresa.class)
    @OneToMany(mappedBy = "usuario")
    private Collection<UsuarioEmpresa> usuarioEmpresas;

    public Usuario() {
    }

    public Usuario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Persona getPersonaFisica() {
        return personaFisica;
    }

    public void setPersonaFisica(Persona personaFisica) {
        this.personaFisica = personaFisica;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setUsuarioPerfiles(Collection<UsuarioPerfil> usuarioPerfiles) {
        this.usuarioPerfiles = usuarioPerfiles;
    }

    public Collection<UsuarioPerfil> getUsuarioPerfiles() {
        return usuarioPerfiles;
    }

    public void setUsuarioLocales(Collection<UsuarioLocal> usuarioLocales) {
        this.usuarioLocales = usuarioLocales;
    }

    public Collection<UsuarioLocal> getUsuarioLocales() {
        return usuarioLocales;
    }

    public void setUsuarioEmpresas(Collection<UsuarioEmpresa> usuarioEmpresas) {
        this.usuarioEmpresas = usuarioEmpresas;
    }

    public Collection<UsuarioEmpresa> getUsuarioEmpresas() {
        return usuarioEmpresas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.usuario.Usuario[ id=" + id + " ]";
    }

}
