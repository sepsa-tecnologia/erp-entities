/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.CanalVenta;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.TipoDatoPersona;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "cliente", catalog = "erp", schema = "comercial")
@XmlRootElement
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_comercial")
    private Integer idComercial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "porcentaje_retencion")
    private Integer porcentajeRetencion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social")
    private String razonSocial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Column(name = "id_naturaleza_cliente")
    private Integer idNaturalezaCliente;
    @Column(name = "id_canal_venta")
    private Integer idCanalVenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_documento")
    private String nroDocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "clave_pago")
    private String clavePago;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fact_electronica")
    private Character factElectronica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proveedor")
    private Character proveedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "comprador")
    private Character comprador;
    @Column(name = "codigo_pais")
    private String codigoPais;
    @JoinColumn(name = "id_canal_venta", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private CanalVenta canalVenta;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDatoPersona tipoDocumento;
    @JoinColumn(name = "id_naturaleza_cliente", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private NaturalezaCliente naturalezaCliente;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = Persona.class)
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;

    public Cliente() {
    }

    public Cliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Cliente(Integer idCliente, Integer idEstado, String razonSocial, int idTipoDocumento, String nroDocumento, String clavePago, Character factElectronica) {
        this.idCliente = idCliente;
        this.idEstado = idEstado;
        this.razonSocial = razonSocial;
        this.idTipoDocumento = idTipoDocumento;
        this.nroDocumento = nroDocumento;
        this.clavePago = clavePago;
        this.factElectronica = factElectronica;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setTipoDocumento(TipoDatoPersona tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public TipoDatoPersona getTipoDocumento() {
        return tipoDocumento;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Persona getPersona() {
        return persona;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getClavePago() {
        return clavePago;
    }

    public void setClavePago(String clavePago) {
        this.clavePago = clavePago;
    }

    public Character getFactElectronica() {
        return factElectronica;
    }

    public void setFactElectronica(Character factElectronica) {
        this.factElectronica = factElectronica;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public Character getProveedor() {
        return proveedor;
    }

    public void setProveedor(Character proveedor) {
        this.proveedor = proveedor;
    }

    public Character getComprador() {
        return comprador;
    }

    public void setComprador(Character comprador) {
        this.comprador = comprador;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setNaturalezaCliente(NaturalezaCliente naturalezaCliente) {
        this.naturalezaCliente = naturalezaCliente;
    }

    public NaturalezaCliente getNaturalezaCliente() {
        return naturalezaCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCliente != null ? idCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.idCliente == null && other.idCliente != null) || (this.idCliente != null && !this.idCliente.equals(other.idCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.Cliente[ idCliente=" + idCliente + " ]";
    }
    
}
