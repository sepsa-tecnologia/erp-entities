/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "contrato_servicio_tarifa", catalog = "erp", schema = "comercial")
@XmlRootElement
public class ContratoServicioTarifa implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContratoServicioTarifaPK contratoServicioTarifaPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;

    public ContratoServicioTarifa() {
    }

    public ContratoServicioTarifa(ContratoServicioTarifaPK contratoServicioTarifaPK) {
        this.contratoServicioTarifaPK = contratoServicioTarifaPK;
    }

    public ContratoServicioTarifa(ContratoServicioTarifaPK contratoServicioTarifaPK, Character activo) {
        this.contratoServicioTarifaPK = contratoServicioTarifaPK;
        this.activo = activo;
    }

    public ContratoServicioTarifa(int idContrato, int idServicio, int idTarifa) {
        this.contratoServicioTarifaPK = new ContratoServicioTarifaPK(idContrato, idServicio, idTarifa);
    }

    public ContratoServicioTarifaPK getContratoServicioTarifaPK() {
        return contratoServicioTarifaPK;
    }

    public void setContratoServicioTarifaPK(ContratoServicioTarifaPK contratoServicioTarifaPK) {
        this.contratoServicioTarifaPK = contratoServicioTarifaPK;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contratoServicioTarifaPK != null ? contratoServicioTarifaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoServicioTarifa)) {
            return false;
        }
        ContratoServicioTarifa other = (ContratoServicioTarifa) object;
        if ((this.contratoServicioTarifaPK == null && other.contratoServicioTarifaPK != null) || (this.contratoServicioTarifaPK != null && !this.contratoServicioTarifaPK.equals(other.contratoServicioTarifaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.ContratoServicioTarifa[ contratoServicioTarifaPK=" + contratoServicioTarifaPK + " ]";
    }
    
}
