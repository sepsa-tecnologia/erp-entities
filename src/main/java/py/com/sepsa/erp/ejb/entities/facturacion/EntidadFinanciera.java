/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Persona;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "entidad_financiera", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class EntidadFinanciera implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;
    @JoinColumn(name = "id_entidad_financiera", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona entidadFinanciera;

    public EntidadFinanciera() {
    }

    public EntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public void setEntidadFinanciera(Persona entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public Persona getEntidadFinanciera() {
        return entidadFinanciera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntidadFinanciera != null ? idEntidadFinanciera.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntidadFinanciera)) {
            return false;
        }
        EntidadFinanciera other = (EntidadFinanciera) object;
        if ((this.idEntidadFinanciera == null && other.idEntidadFinanciera != null) || (this.idEntidadFinanciera != null && !this.idEntidadFinanciera.equals(other.idEntidadFinanciera))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.EntidadFinanciera[ idEntidadFinanciera=" + idEntidadFinanciera + " ]";
    }
    
}
