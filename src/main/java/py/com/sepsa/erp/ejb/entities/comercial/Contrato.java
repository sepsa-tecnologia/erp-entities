/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import py.com.sepsa.erp.ejb.entities.info.Aprobacion;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "contrato", catalog = "erp", schema = "comercial")
@XmlRootElement
public class Contrato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_contrato")
    private String nroContrato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "firmado")
    private Character firmado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "documento_aval")
    private Character documentoAval;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renovacion_automatica")
    private Character renovacionAutomatica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "debito_automatico")
    private Character debitoAutomatico;
    @Column(name = "plazo_rescision")
    private Integer plazoRescision;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cobro_adelantado")
    private Character cobroAdelantado;
    @NotNull
    @Column(name = "id_cliente")
    private Integer idCliente;
    @NotNull
    @Column(name = "id_aprobacion")
    private Integer idAprobacion;
    @NotNull
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @NotNull
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "id_periodo_cobro")
    private Integer idPeriodoCobro;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProductoCom producto;
    @JoinColumn(name = "id_periodo_cobro", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private PeriodoCobro periodoCobro;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Moneda moneda;
    @JoinColumn(name = "id_aprobacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Aprobacion aprobacion;

    public Contrato() {
    }

    public Contrato(Integer id) {
        this.id = id;
    }

    public Contrato(Integer id, String nroContrato, Date fechaInicio, Date fechaFin, Character firmado, Character documentoAval, Character renovacionAutomatica, Character debitoAutomatico, int idEstado, Character cobroAdelantado) {
        this.id = id;
        this.nroContrato = nroContrato;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.firmado = firmado;
        this.documentoAval = documentoAval;
        this.renovacionAutomatica = renovacionAutomatica;
        this.debitoAutomatico = debitoAutomatico;
        this.idEstado = idEstado;
        this.cobroAdelantado = cobroAdelantado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Character getFirmado() {
        return firmado;
    }

    public void setFirmado(Character firmado) {
        this.firmado = firmado;
    }

    public Character getDocumentoAval() {
        return documentoAval;
    }

    public void setDocumentoAval(Character documentoAval) {
        this.documentoAval = documentoAval;
    }

    public Character getRenovacionAutomatica() {
        return renovacionAutomatica;
    }

    public void setRenovacionAutomatica(Character renovacionAutomatica) {
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Character getDebitoAutomatico() {
        return debitoAutomatico;
    }

    public void setDebitoAutomatico(Character debitoAutomatico) {
        this.debitoAutomatico = debitoAutomatico;
    }

    public Integer getPlazoRescision() {
        return plazoRescision;
    }

    public void setPlazoRescision(Integer plazoRescision) {
        this.plazoRescision = plazoRescision;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdAprobacion() {
        return idAprobacion;
    }

    public void setIdAprobacion(Integer idAprobacion) {
        this.idAprobacion = idAprobacion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdPeriodoCobro() {
        return idPeriodoCobro;
    }

    public void setIdPeriodoCobro(Integer idPeriodoCobro) {
        this.idPeriodoCobro = idPeriodoCobro;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ProductoCom getProducto() {
        return producto;
    }

    public void setProducto(ProductoCom producto) {
        this.producto = producto;
    }

    public PeriodoCobro getPeriodoCobro() {
        return periodoCobro;
    }

    public void setPeriodoCobro(PeriodoCobro periodoCobro) {
        this.periodoCobro = periodoCobro;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Aprobacion getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(Aprobacion aprobacion) {
        this.aprobacion = aprobacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.Contrato[ id=" + id + " ]";
    }
    
}
