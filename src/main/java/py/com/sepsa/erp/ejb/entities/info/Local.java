/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioLocal;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "local", catalog = "erp", schema = "info")
@XmlRootElement
public class Local implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "gln")
    private BigInteger gln;
    @Size(max = 2147483647)
    @Column(name = "id_externo")
    private String idExterno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "id_direccion")
    private Integer idDireccion;
    @NotNull
    @Column(name = "id_persona")
    private Integer idPersona;
    @Column(name = "id_telefono")
    private Integer idTelefono;
    @Column(name = "local_externo")
    private Character localExterno;
    @JoinColumn(name = "id_direccion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Direccion direccion;
    @JoinColumn(name = "id_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;
    @JoinColumn(name = "id_telefono", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = true)
    private Telefono telefono;
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_persona", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @GsonRepellent
    @OneToMany(mappedBy = "local")
    private Collection<UsuarioLocal> usuarioLocales;

    public Local() {
    }

    public Local(Integer id) {
        this.id = id;
    }

    public Local(Integer id, BigInteger gln, Character activo, String descripcion) {
        this.id = id;
        this.gln = gln;
        this.activo = activo;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getGln() {
        return gln;
    }

    public void setGln(BigInteger gln) {
        this.gln = gln;
    }

    public String getIdExterno() {
        return idExterno;
    }

    public void setIdExterno(String idExterno) {
        this.idExterno = idExterno;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setLocalExterno(Character localExterno) {
        this.localExterno = localExterno;
    }

    public Character getLocalExterno() {
        return localExterno;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setUsuarioLocales(Collection<UsuarioLocal> usuarioLocales) {
        this.usuarioLocales = usuarioLocales;
    }

    public Collection<UsuarioLocal> getUsuarioLocales() {
        return usuarioLocales;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local)) {
            return false;
        }
        Local other = (Local) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.Local[ id=" + id + " ]";
    }
    
}
