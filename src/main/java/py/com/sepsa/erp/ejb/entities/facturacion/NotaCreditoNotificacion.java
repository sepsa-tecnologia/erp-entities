/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.TipoNotificacion;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "nota_credito_notificacion", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class NotaCreditoNotificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_nota_credito")
    private Integer idNotaCredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_notificacion")
    private Integer idTipoNotificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "email")
    private String email;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = NotaCredito.class)
    @JoinColumn(name = "id_nota_credito", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NotaCredito notaCredito;
    @JoinColumn(name = "id_tipo_notificacion", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private TipoNotificacion tipoNotificacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setIdNotaCredito(Integer idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public NotaCredito getNotaCredito() {
        return notaCredito;
    }

    public void setNotaCredito(NotaCredito notaCredito) {
        this.notaCredito = notaCredito;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoNotificacion getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(TipoNotificacion tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaCreditoNotificacion)) {
            return false;
        }
        NotaCreditoNotificacion other = (NotaCreditoNotificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.NotaCreditoNotificacion[ id=" + id + " ]";
    }
    
}
