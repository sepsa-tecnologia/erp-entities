/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "detalle_procesamiento", catalog = "erp", schema = "set")
@XmlRootElement
public class DetalleProcesamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo_resultado")
    private Integer codigoResultado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "mensaje_resultado")
    private String mensajeResultado;
    @NotNull
    @Column(name = "id_procesamiento")
    private Integer idProcesamiento;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = Procesamiento.class)
    @JoinColumn(name = "id_procesamiento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Procesamiento procesamiento;

    public DetalleProcesamiento() {
    }

    public DetalleProcesamiento(Integer id) {
        this.id = id;
    }

    public DetalleProcesamiento(Integer id, Integer codigoResultado, String mensajeResultado) {
        this.id = id;
        this.codigoResultado = codigoResultado;
        this.mensajeResultado = mensajeResultado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigoResultado() {
        return codigoResultado;
    }

    public void setCodigoResultado(Integer codigoResultado) {
        this.codigoResultado = codigoResultado;
    }

    public String getMensajeResultado() {
        return mensajeResultado;
    }

    public void setMensajeResultado(String mensajeResultado) {
        this.mensajeResultado = mensajeResultado;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Procesamiento getProcesamiento() {
        return procesamiento;
    }

    public void setProcesamiento(Procesamiento procesamiento) {
        this.procesamiento = procesamiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleProcesamiento)) {
            return false;
        }
        DetalleProcesamiento other = (DetalleProcesamiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.setserver.ejb.entities.trans.DetalleProcesamiento[ id=" + id + " ]";
    }
    
}
