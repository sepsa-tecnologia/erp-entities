/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Williams Vera
 */
@Entity
@Table(name = "vehiculo_traslado", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class VehiculoTraslado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "tipo_vehiculo")
    private String tipoVehiculo;
    @NotNull
    @Column(name = "marca")
    private String marca;
    @Column(name = "tipo_identificacion")
    private Integer tipoIdentificacion;
    @Column(name = "nro_identificacion")
    private String nroIdentificacion;
    @Column(name = "dato_adicional")
    private String datoAdicional;
    @Column(name = "nro_matricula")
    private String nroMatricula;
    @Column(name = "nro_vuelo")
    private String nroVuelo;
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "activo")
    private Character activo;
    
    public VehiculoTraslado() {
    }

    public VehiculoTraslado(Integer id) {
        this.id = id;
    }

    public VehiculoTraslado(Integer id, String tipoVehiculo, String marca, Integer tipoIdentificacion ) {
        this.id = id;
        this.tipoVehiculo = tipoVehiculo;
        this.marca = marca;
        this.tipoIdentificacion = tipoIdentificacion;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Integer tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNroIdentificacion() {
        return nroIdentificacion;
    }

    public void setNroIdentificacion(String nroIdentificacion) {
        this.nroIdentificacion = nroIdentificacion;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getNroMatricula() {
        return nroMatricula;
    }

    public void setNroMatricula(String nroMatricula) {
        this.nroMatricula = nroMatricula;
    }

    public String getNroVuelo() {
        return nroVuelo;
    }

    public void setNroVuelo(String nroVuelo) {
        this.nroVuelo = nroVuelo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehiculoTraslado)) {
            return false;
        }
        VehiculoTraslado other = (VehiculoTraslado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.VehiculoTraslado[ id=" + id + " ]";
    }
    
}
