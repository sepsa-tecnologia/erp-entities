/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "transaccion_red_pago", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class TransaccionRedPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id_transaccion")
    private String idTransaccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @NotNull
    @Column(name = "id_mensaje")
    private Integer idMensaje;
    @NotNull
    @Column(name = "id_operacion")
    private Integer idOperacion;
    @NotNull
    @Column(name = "id_red")
    private Integer idRed;
    @JoinColumn(name = "id_mensaje", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MensajeRedPago mensaje;
    @JoinColumn(name = "id_operacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private OperacionRedPago operacion;
    @JoinColumn(name = "id_red", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private RedPago red;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;

    public TransaccionRedPago() {
    }

    public TransaccionRedPago(Integer id) {
        this.id = id;
    }

    public TransaccionRedPago(Integer id, String idTransaccion, Date fecha) {
        this.id = id;
        this.idTransaccion = idTransaccion;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Integer getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(Integer idOperacion) {
        this.idOperacion = idOperacion;
    }

    public Integer getIdRed() {
        return idRed;
    }

    public void setIdRed(Integer idRed) {
        this.idRed = idRed;
    }

    public MensajeRedPago getMensaje() {
        return mensaje;
    }

    public void setMensaje(MensajeRedPago mensaje) {
        this.mensaje = mensaje;
    }

    public OperacionRedPago getOperacion() {
        return operacion;
    }

    public void setOperacion(OperacionRedPago operacion) {
        this.operacion = operacion;
    }

    public RedPago getRed() {
        return red;
    }

    public void setRed(RedPago red) {
        this.red = red;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransaccionRedPago)) {
            return false;
        }
        TransaccionRedPago other = (TransaccionRedPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.TransaccionRedPago[ id=" + id + " ]";
    }
    
}
