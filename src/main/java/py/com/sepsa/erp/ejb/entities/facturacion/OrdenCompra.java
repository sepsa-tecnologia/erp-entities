/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "orden_compra", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class OrdenCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado")
    private Integer idEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_recepcion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_orden_compra")
    private String nroOrdenCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "recibido")
    private Character recibido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "registro_inventario")
    private Character registroInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "archivo_edi")
    private Character archivoEdi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "generado_edi")
    private Character generadoEdi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_5")
    private BigDecimal montoIva5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_5")
    private BigDecimal montoImponible5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_5")
    private BigDecimal montoTotal5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_10")
    private BigDecimal montoIva10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_10")
    private BigDecimal montoImponible10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_10")
    private BigDecimal montoTotal10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_exento")
    private BigDecimal montoTotalExento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_orden_compra")
    private BigDecimal montoTotalOrdenCompra;
    @Column(name = "id_cliente_origen")
    private Integer idClienteOrigen;
    @Column(name = "id_local_origen")
    private Integer idLocalOrigen;
    @Column(name = "id_cliente_destino")
    private Integer idClienteDestino;
    @Column(name = "id_local_destino")
    private Integer idLocalDestino;
    @Column(name = "id_procesamiento_archivo")
    private Integer idProcesamientoArchivo;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "id_cliente_origen", referencedColumnName = "id_cliente", updatable = false, insertable = false)
    @ManyToOne
    private Cliente clienteOrigen;
    @JoinColumn(name = "id_local_origen", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localOrigen;
    @JoinColumn(name = "id_cliente_destino", referencedColumnName = "id_cliente", updatable = false, insertable = false)
    @ManyToOne
    private Cliente clienteDestino;
    @JoinColumn(name = "id_local_destino", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localDestino;
    @JoinColumn(name = "id_procesamiento_archivo", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private ProcesamientoArchivo procesamientoArchivo;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = OrdenCompraDetalle.class)
    @OneToMany(mappedBy = "ordenCompra")
    private Collection<OrdenCompraDetalle> ordenCompraDetalles;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;

    public OrdenCompra() {
    }

    public OrdenCompra(Integer id) {
        this.id = id;
    }

    public OrdenCompra(Integer id, Integer idEstado, Date fechaRecepcion, Date fechaInsercion, String nroOrdenCompra, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalOrdenCompra) {
        this.id = id;
        this.idEstado = idEstado;
        this.fechaRecepcion = fechaRecepcion;
        this.fechaInsercion = fechaInsercion;
        this.nroOrdenCompra = nroOrdenCompra;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalOrdenCompra = montoTotalOrdenCompra;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getNroOrdenCompra() {
        return nroOrdenCompra;
    }

    public void setNroOrdenCompra(String nroOrdenCompra) {
        this.nroOrdenCompra = nroOrdenCompra;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public void setMontoTotalOrdenCompra(BigDecimal montoTotalOrdenCompra) {
        this.montoTotalOrdenCompra = montoTotalOrdenCompra;
    }

    public BigDecimal getMontoTotalOrdenCompra() {
        return montoTotalOrdenCompra;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Collection<OrdenCompraDetalle> getOrdenCompraDetalles() {
        return ordenCompraDetalles;
    }

    public void setOrdenCompraDetalles(Collection<OrdenCompraDetalle> ordenCompraDetalles) {
        this.ordenCompraDetalles = ordenCompraDetalles;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public Integer getIdClienteOrigen() {
        return idClienteOrigen;
    }

    public void setIdClienteOrigen(Integer idClienteOrigen) {
        this.idClienteOrigen = idClienteOrigen;
    }

    public Integer getIdClienteDestino() {
        return idClienteDestino;
    }

    public void setIdClienteDestino(Integer idClienteDestino) {
        this.idClienteDestino = idClienteDestino;
    }

    public Cliente getClienteOrigen() {
        return clienteOrigen;
    }

    public void setClienteOrigen(Cliente clienteOrigen) {
        this.clienteOrigen = clienteOrigen;
    }

    public Cliente getClienteDestino() {
        return clienteDestino;
    }

    public void setClienteDestino(Cliente clienteDestino) {
        this.clienteDestino = clienteDestino;
    }

    public void setProcesamientoArchivo(ProcesamientoArchivo procesamientoArchivo) {
        this.procesamientoArchivo = procesamientoArchivo;
    }

    public ProcesamientoArchivo getProcesamientoArchivo() {
        return procesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public void setRecibido(Character recibido) {
        this.recibido = recibido;
    }

    public Character getRecibido() {
        return recibido;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getAnulado() {
        return anulado;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setRegistroInventario(Character registroInventario) {
        this.registroInventario = registroInventario;
    }

    public Character getRegistroInventario() {
        return registroInventario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenCompra)) {
            return false;
        }
        OrdenCompra other = (OrdenCompra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra[ id=" + id + " ]";
    }
    
}
