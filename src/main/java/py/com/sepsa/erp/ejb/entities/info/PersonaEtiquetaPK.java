/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jonathan
 */
@Embeddable
public class PersonaEtiquetaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_etiqueta")
    private int idEtiqueta;

    public PersonaEtiquetaPK() {
    }

    public PersonaEtiquetaPK(int idPersona, int idEtiqueta) {
        this.idPersona = idPersona;
        this.idEtiqueta = idEtiqueta;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(int idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPersona;
        hash += (int) idEtiqueta;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaEtiquetaPK)) {
            return false;
        }
        PersonaEtiquetaPK other = (PersonaEtiquetaPK) object;
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idEtiqueta != other.idEtiqueta) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.PersonaEtiquetaPK[ idPersona=" + idPersona + ", idEtiqueta=" + idEtiqueta + " ]";
    }
    
}
