/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "factura_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class FacturaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_factura")
    private Integer idFactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "dato_adicional")
    private String datoAdicional;
    @Column(name = "cod_dncp_nivel_general")
    private String codDncpNivelGeneral;
    @Column(name = "cod_dncp_nivel_especifico")
    private String codDncpNivelEspecifico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_iva")
    private Integer porcentajeIva;
    @Column(name = "porcentaje_gravada")
    private BigDecimal porcentajeGravada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private BigDecimal cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_con_iva")
    private BigDecimal precioUnitarioConIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_unitario_sin_iva")
    private BigDecimal precioUnitarioSinIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descuento_particular_unitario")
    private BigDecimal descuentoParticularUnitario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "descuento_global_unitario")
    private BigDecimal descuentoGlobalUnitario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva")
    private BigDecimal montoIva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible")
    private BigDecimal montoImponible;
    @Column(name = "monto_exento_gravado")
    private BigDecimal montoExentoGravado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_particular")
    private BigDecimal montoDescuentoParticular;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_descuento_global")
    private BigDecimal montoDescuentoGlobal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total")
    private BigDecimal montoTotal;
    @Column(name = "id_liquidacion")
    private Integer idLiquidacion;
    @Column(name = "id_servicio")
    private Integer idServicio;
    @Column(name = "id_producto")
    private Integer idProducto;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = Factura.class)
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    @JoinColumn(name = "id_liquidacion", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Liquidacion liquidacion;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Servicio servicio;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Producto producto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setPorcentajeGravada(BigDecimal porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public BigDecimal getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacturaDetalle)) {
            return false;
        }
        FacturaDetalle other = (FacturaDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.FacturaDetalle[ id=" + id + " ]";
    }
    
}
