/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaDetalle;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Motivo;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "devolucion_detalle", catalog = "erp", schema = "inventario")
@XmlRootElement
public class DevolucionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_devolucion")
    private Integer idDevolucion;
    @Column(name = "id_factura_detalle")
    private Integer idFacturaDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_motivo")
    private Integer idMotivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado_inventario")
    private Integer idEstadoInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_deposito_logistico")
    private Integer idDepositoLogistico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @NotNull
    @Column(name = "cantidad_facturada")
    private Integer cantidadFacturada;
    @NotNull
    @Column(name = "cantidad_devolucion")
    private Integer cantidadDevolucion;
    @Size(min = 1, max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = Devolucion.class)
    @JoinColumn(name = "id_devolucion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Devolucion devolucion;
    @JoinColumn(name = "id_motivo", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Motivo motivo;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "id_estado_inventario", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Estado estadoInventario;
    @JoinColumn(name = "id_deposito_logistico", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private DepositoLogistico depositoLogistico;
    @JoinColumn(name = "id_deposito_logistico", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private FacturaDetalle facturaDetalle;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdDevolucion(Integer idDevolucion) {
        this.idDevolucion = idDevolucion;
    }

    public Integer getIdDevolucion() {
        return idDevolucion;
    }

    public Integer getIdFacturaDetalle() {
        return idFacturaDetalle;
    }

    public void setIdFacturaDetalle(Integer idFacturaDetalle) {
        this.idFacturaDetalle = idFacturaDetalle;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Integer getCantidadFacturada() {
        return cantidadFacturada;
    }

    public void setCantidadFacturada(Integer cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }

    public Integer getCantidadDevolucion() {
        return cantidadDevolucion;
    }

    public void setCantidadDevolucion(Integer cantidadDevolucion) {
        this.cantidadDevolucion = cantidadDevolucion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Devolucion getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Devolucion devolucion) {
        this.devolucion = devolucion;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public Estado getEstadoInventario() {
        return estadoInventario;
    }

    public void setEstadoInventario(Estado estadoInventario) {
        this.estadoInventario = estadoInventario;
    }

    public DepositoLogistico getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(DepositoLogistico depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public FacturaDetalle getFacturaDetalle() {
        return facturaDetalle;
    }

    public void setFacturaDetalle(FacturaDetalle facturaDetalle) {
        this.facturaDetalle = facturaDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DevolucionDetalle)) {
            return false;
        }
        DevolucionDetalle other = (DevolucionDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.FacturaDetalle[ id=" + id + " ]";
    }
    
}
