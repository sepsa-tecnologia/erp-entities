/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "nota_credito", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class NotaCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Column(name = "id_naturaleza_cliente")
    private Integer idNaturalezaCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_nota_credito")
    private String nroNotaCredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social")
    private String razonSocial;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "ruc")
    private String ruc;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "digital")
    private Character digital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "impreso")
    private Character impreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entregado")
    private Character entregado;
    @Column(name = "archivo_edi")
    private Character archivoEdi;
    @Column(name = "archivo_set")
    private Character archivoSet;
    @Column(name = "generado_edi")
    private Character generadoEdi;
    @Column(name = "generado_set")
    private Character generadoSet;
    @Column(name = "estado_sincronizado")
    private Character estadoSincronizado;
    @Column(name = "fecha_entrega")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrega;
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_5")
    private BigDecimal montoIva5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_5")
    private BigDecimal montoImponible5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_5")
    private BigDecimal montoTotal5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_10")
    private BigDecimal montoIva10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_10")
    private BigDecimal montoImponible10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_10")
    private BigDecimal montoTotal10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_exento")
    private BigDecimal montoTotalExento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_nota_credito")
    private BigDecimal montoTotalNotaCredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_guaranies")
    private BigDecimal montoTotalGuaranies;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_talonario")
    private Integer idTalonario;
    @Column(name = "id_encargado")
    private Integer idEncargado;
    @Column(name = "id_motivo_emision")
    private Integer idMotivoEmision;
    @Column(name = "id_motivo_emision_interno")
    private Integer idMotivoEmisionInterno;
    @Size(max = 2147483647)
    @Column(name = "cdc")
    private String cdc;
    @Size(max = 2147483647)
    @Column(name = "cod_seguridad")
    private String codSeguridad;
    @Column(name = "nro_casa")
    private Integer nroCasa;
    @Column(name = "id_departamento")
    private Integer idDepartamento;
    @Column(name = "id_distrito")
    private Integer idDistrito;
    @Column(name = "id_ciudad")
    private Integer idCiudad;
    @Size(max = 2147483647)
    @Column(name = "serie")
    private String serie;
    @Column(name = "id_motivo_anulacion")
    private Integer idMotivoAnulacion;
    @Column(name = "observacion_anulacion")
    private String observacionAnulacion;
    @Column(name = "id_estado")
    private Integer idEstado;
    @Column(name = "id_procesamiento")
    private Integer idProcesamiento;
    @Column(name = "monto_total_descuento_global")
    private BigDecimal montoTotalDescuentoGlobal;
    @Column(name = "porcentaje_descuento_global")
    private BigDecimal porcentajeDescuentoGlobal;
    @Column(name = "id_tipo_cambio")
    private Integer idTipoCambio;
    @JoinColumn(name = "id_encargado", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Usuario encargado;
    @JoinColumn(name = "id_motivo_emision", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MotivoEmision motivoEmision;
    @JoinColumn(name = "id_motivo_emision_interno", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MotivoEmisionInterno motivoEmisionInterno;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Moneda moneda;
    @JoinColumn(name = "id_naturaleza_cliente", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private NaturalezaCliente naturalezaCliente;
    @JoinColumn(name = "id_motivo_anulacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MotivoAnulacion motivoAnulacion;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Talonario talonario;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_estado", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Estado estado;
    @JoinColumn(name = "id_procesamiento", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Procesamiento procesamiento;
    @JoinColumn(name = "id_tipo_cambio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private TipoCambio tipoCambio;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = NotaCreditoDetalle.class)
    @OneToMany(mappedBy = "notaCredito")
    private Collection<NotaCreditoDetalle> notaCreditoDetalles;

    public NotaCredito() {
    }

    public NotaCredito(Integer id) {
        this.id = id;
    }

    public NotaCredito(Integer id, String nroNotaCredito, Date fecha, String razonSocial, Character anulado, Character impreso, Character entregado, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalNotaCredito) {
        this.id = id;
        this.nroNotaCredito = nroNotaCredito;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.anulado = anulado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public void setNotaCreditoDetalles(Collection<NotaCreditoDetalle> notaCreditoDetalles) {
        this.notaCreditoDetalles = notaCreditoDetalles;
    }

    public Collection<NotaCreditoDetalle> getNotaCreditoDetalles() {
        return notaCreditoDetalles;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public MotivoEmision getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(MotivoEmision motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public Integer getIdMotivoEmisionInterno() {
        return idMotivoEmisionInterno;
    }

    public void setIdMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
    }

    public MotivoEmisionInterno getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setProcesamiento(Procesamiento procesamiento) {
        this.procesamiento = procesamiento;
    }

    public Procesamiento getProcesamiento() {
        return procesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setNaturalezaCliente(NaturalezaCliente naturalezaCliente) {
        this.naturalezaCliente = naturalezaCliente;
    }

    public NaturalezaCliente getNaturalezaCliente() {
        return naturalezaCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getPorcentajeDescuentoGlobal() {
        return porcentajeDescuentoGlobal;
    }

    public void setPorcentajeDescuentoGlobal(BigDecimal porcentajeDescuentoGlobal) {
        this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaCredito)) {
            return false;
        }
        NotaCredito other = (NotaCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.NotaCredito[ id=" + id + " ]";
    }
    
}
