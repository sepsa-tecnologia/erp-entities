/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.utils.gson.GsonRepellent;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "factura_compra", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class FacturaCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_persona")
    private Integer idPersona;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_factura")
    private Integer idTipoFactura;
    @Column(name = "id_orden_compra")
    private Integer idOrdenCompra;
    @Column(name = "id_operacion_inventario")
    private Integer idOperacionInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_timbrado")
    private String nroTimbrado;
    @Column(name = "fecha_vencimiento_timbrado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimientoTimbrado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nro_factura")
    private String nroFactura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "razon_social")
    private String razonSocial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ruc")
    private String ruc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "digital")
    private Character digital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pagado")
    private Character pagado;
    @Column(name = "cdc")
    private String cdc;
    @Column(name = "dias_credito")
    private Integer diasCredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_5")
    private BigDecimal montoIva5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_5")
    private BigDecimal montoImponible5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_5")
    private BigDecimal montoTotal5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_10")
    private BigDecimal montoIva10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_10")
    private BigDecimal montoImponible10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_10")
    private BigDecimal montoTotal10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_exento")
    private BigDecimal montoTotalExento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_iva_total")
    private BigDecimal montoIvaTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_imponible_total")
    private BigDecimal montoImponibleTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_total_factura")
    private BigDecimal montoTotalFactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldo")
    private BigDecimal saldo;
    @Column(name = "id_motivo_anulacion")
    private Integer idMotivoAnulacion;
    @Column(name = "id_procesamiento_archivo")
    private Integer idProcesamientoArchivo;
    @Column(name = "observacion_anulacion")
    private String observacionAnulacion;
    @Column(name = "id_local_origen")
    private Integer idLocalOrigen;
    @Column(name = "id_local_destino")
    private Integer idLocalDestino;
    @JoinColumn(name = "id_orden_compra", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private OrdenCompra ordenCompra;
    @JoinColumn(name = "id_tipo_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoFactura tipoFactura;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Moneda moneda;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;
    @JoinColumn(name = "id_motivo_anulacion", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private MotivoAnulacion motivoAnulacion;
    @JoinColumn(name = "id_local_origen", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localOrigen;
    @JoinColumn(name = "id_local_destino", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private Local localDestino;
    @JoinColumn(name = "id_procesamiento_archivo", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne
    private ProcesamientoArchivo procesamientoArchivo;
    @GsonRepellent(onlyCyclicReference = true, cyclicReferenceClass = FacturaCompraDetalle.class)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFacturaCompra")
    private Collection<FacturaCompraDetalle> facturaCompraDetalles;

    public FacturaCompra() {
    }

    public FacturaCompra(Integer id) {
        this.id = id;
    }

    public FacturaCompra(Integer id, Integer idEmpresa, Integer idMoneda, Date fechaInsercion, Date fecha, String nroFactura, String razonSocial, String ruc, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalFactura) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.idMoneda = idMoneda;
        this.fechaInsercion = fechaInsercion;
        this.fecha = fecha;
        this.nroFactura = nroFactura;
        this.razonSocial = razonSocial;
        this.ruc = ruc;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalFactura = montoTotalFactura;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(Integer diasCredito) {
        this.diasCredito = diasCredito;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public OrdenCompra getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(OrdenCompra ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public TipoFactura getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(TipoFactura tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getAnulado() {
        return anulado;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setPagado(Character pagado) {
        this.pagado = pagado;
    }

    public Character getPagado() {
        return pagado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public void setProcesamientoArchivo(ProcesamientoArchivo procesamientoArchivo) {
        this.procesamientoArchivo = procesamientoArchivo;
    }

    public ProcesamientoArchivo getProcesamientoArchivo() {
        return procesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public void setIdOperacionInventario(Integer idOperacionInventario) {
        this.idOperacionInventario = idOperacionInventario;
    }

    public Integer getIdOperacionInventario() {
        return idOperacionInventario;
    }

    @XmlTransient
    public Collection<FacturaCompraDetalle> getFacturaCompraDetalles() {
        return facturaCompraDetalles;
    }

    public void setFacturaCompraDetalles(Collection<FacturaCompraDetalle> facturaCompraDetalles) {
        this.facturaCompraDetalles = facturaCompraDetalles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacturaCompra)) {
            return false;
        }
        FacturaCompra other = (FacturaCompra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra[ id=" + id + " ]";
    }
    
}
