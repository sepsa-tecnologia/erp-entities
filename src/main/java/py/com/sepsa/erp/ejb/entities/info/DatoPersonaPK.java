/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jonathan
 */
@Embeddable
public class DatoPersonaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipo_dato_persona")
    private int idTipoDatoPersona;

    public DatoPersonaPK() {
    }

    public DatoPersonaPK(int idPersona, int idTipoDatoPersona) {
        this.idPersona = idPersona;
        this.idTipoDatoPersona = idTipoDatoPersona;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdTipoDatoPersona() {
        return idTipoDatoPersona;
    }

    public void setIdTipoDatoPersona(int idTipoDatoPersona) {
        this.idTipoDatoPersona = idTipoDatoPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPersona;
        hash += (int) idTipoDatoPersona;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoPersonaPK)) {
            return false;
        }
        DatoPersonaPK other = (DatoPersonaPK) object;
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idTipoDatoPersona != other.idTipoDatoPersona) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.DatoPersonaPK[ idPersona=" + idPersona + ", idTipoDatoPersona=" + idTipoDatoPersona + " ]";
    }
    
}
