/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "contrato_servicio", catalog = "erp", schema = "comercial")
@XmlRootElement
public class ContratoServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContratoServicioPK contratoServicioPK;
    @Column(name = "monto_acuerdo")
    private BigDecimal montoAcuerdo;
    @Column(name = "fecha_vencimiento_acuerdo")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimientoAcuerdo;
    @Column(name = "id_monto_minimo")
    private Integer idMontoMinimo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contrato contrato;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicio servicio;
    @JoinColumn(name = "id_monto_minimo", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private MontoMinimo montoMinimo;

    public ContratoServicio() {
    }

    public ContratoServicio(ContratoServicioPK contratoServicioPK) {
        this.contratoServicioPK = contratoServicioPK;
    }

    public ContratoServicio(ContratoServicioPK contratoServicioPK, Character activo) {
        this.contratoServicioPK = contratoServicioPK;
        this.activo = activo;
    }

    public ContratoServicio(int idContrato, int idServicio) {
        this.contratoServicioPK = new ContratoServicioPK(idContrato, idServicio);
    }

    public ContratoServicioPK getContratoServicioPK() {
        return contratoServicioPK;
    }

    public void setContratoServicioPK(ContratoServicioPK contratoServicioPK) {
        this.contratoServicioPK = contratoServicioPK;
    }

    public BigDecimal getMontoAcuerdo() {
        return montoAcuerdo;
    }

    public void setMontoAcuerdo(BigDecimal montoAcuerdo) {
        this.montoAcuerdo = montoAcuerdo;
    }

    public Date getFechaVencimientoAcuerdo() {
        return fechaVencimientoAcuerdo;
    }

    public void setFechaVencimientoAcuerdo(Date fechaVencimientoAcuerdo) {
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public void setMontoMinimo(MontoMinimo montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public MontoMinimo getMontoMinimo() {
        return montoMinimo;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contratoServicioPK != null ? contratoServicioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoServicio)) {
            return false;
        }
        ContratoServicio other = (ContratoServicio) object;
        if ((this.contratoServicioPK == null && other.contratoServicioPK != null) || (this.contratoServicioPK != null && !this.contratoServicioPK.equals(other.contratoServicioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.ContratoServicio[ contratoServicioPK=" + contratoServicioPK + " ]";
    }
    
}
