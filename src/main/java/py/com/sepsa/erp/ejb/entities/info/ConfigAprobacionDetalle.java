/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "config_aprobacion_detalle", catalog = "erp", schema = "info")
@XmlRootElement
public class ConfigAprobacionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_config_aprobacion")
    private int idConfigAprobacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_etiqueta")
    private int idEtiqueta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "orden")
    private int orden;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;

    public ConfigAprobacionDetalle() {
    }

    public ConfigAprobacionDetalle(Integer id) {
        this.id = id;
    }

    public ConfigAprobacionDetalle(Integer id, int idConfigAprobacion, int idEtiqueta, int orden, Character activo) {
        this.id = id;
        this.idConfigAprobacion = idConfigAprobacion;
        this.idEtiqueta = idEtiqueta;
        this.orden = orden;
        this.activo = activo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdConfigAprobacion() {
        return idConfigAprobacion;
    }

    public void setIdConfigAprobacion(int idConfigAprobacion) {
        this.idConfigAprobacion = idConfigAprobacion;
    }

    public int getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(int idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigAprobacionDetalle)) {
            return false;
        }
        ConfigAprobacionDetalle other = (ConfigAprobacionDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.info.ConfigAprobacionDetalle[ id=" + id + " ]";
    }
    
}
