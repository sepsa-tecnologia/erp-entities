/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "config_bonificacion", catalog = "erp", schema = "comercial")
@XmlRootElement
public class ConfigBonificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentual")
    private Character porcentual;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @JoinColumn(name = "id_producto_bonificado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ProductoCom idProductoBonificado;
    @JoinColumn(name = "id_producto_bonificador", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ProductoCom idProductoBonificador;
    @JoinColumn(name = "id_servicio_bonificado", referencedColumnName = "id")
    @ManyToOne
    private Servicio idServicioBonificado;
    @JoinColumn(name = "id_servicio_bonificador", referencedColumnName = "id")
    @ManyToOne
    private Servicio idServicioBonificador;

    public ConfigBonificacion() {
    }

    public ConfigBonificacion(Integer id) {
        this.id = id;
    }

    public ConfigBonificacion(Integer id, Character activo) {
        this.id = id;
        this.activo = activo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public ProductoCom getIdProductoBonificado() {
        return idProductoBonificado;
    }

    public void setIdProductoBonificado(ProductoCom idProductoBonificado) {
        this.idProductoBonificado = idProductoBonificado;
    }

    public ProductoCom getIdProductoBonificador() {
        return idProductoBonificador;
    }

    public void setIdProductoBonificador(ProductoCom idProductoBonificador) {
        this.idProductoBonificador = idProductoBonificador;
    }

    public Servicio getIdServicioBonificado() {
        return idServicioBonificado;
    }

    public void setIdServicioBonificado(Servicio idServicioBonificado) {
        this.idServicioBonificado = idServicioBonificado;
    }

    public Servicio getIdServicioBonificador() {
        return idServicioBonificador;
    }

    public void setIdServicioBonificador(Servicio idServicioBonificador) {
        this.idServicioBonificador = idServicioBonificador;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigBonificacion)) {
            return false;
        }
        ConfigBonificacion other = (ConfigBonificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.comercial.ConfigBonificacion[ id=" + id + " ]";
    }
    
}
