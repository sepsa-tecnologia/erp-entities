/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "excedente_detalle", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class ExcedenteDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_historico_liquidacion")
    private Integer idHistoricoLiquidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad_excedida")
    private int cantidadExcedida;
    @Column(name = "id_excedente")
    private Integer idExcedente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto_excedente")
    private BigDecimal montoExcedente;
    @JoinColumn(name = "id_excedente", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Excedente excedente;
    @JoinColumn(name = "id_historico_liquidacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private HistoricoLiquidacion historicoLiquidacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public int getCantidadExcedida() {
        return cantidadExcedida;
    }

    public void setCantidadExcedida(int cantidadExcedida) {
        this.cantidadExcedida = cantidadExcedida;
    }

    public BigDecimal getMontoExcedente() {
        return montoExcedente;
    }

    public void setMontoExcedente(BigDecimal montoExcedente) {
        this.montoExcedente = montoExcedente;
    }

    public void setIdExcedente(Integer idExcedente) {
        this.idExcedente = idExcedente;
    }

    public Integer getIdExcedente() {
        return idExcedente;
    }

    public void setExcedente(Excedente excedente) {
        this.excedente = excedente;
    }

    public Excedente getExcedente() {
        return excedente;
    }

    public HistoricoLiquidacion getHistoricoLiquidacion() {
        return historicoLiquidacion;
    }

    public void setHistoricoLiquidacion(HistoricoLiquidacion historicoLiquidacion) {
        this.historicoLiquidacion = historicoLiquidacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExcedenteDetalle)) {
            return false;
        }
        ExcedenteDetalle other = (ExcedenteDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.ExcedenteDetalle[ id=" + id + " ]";
    }
    
}
