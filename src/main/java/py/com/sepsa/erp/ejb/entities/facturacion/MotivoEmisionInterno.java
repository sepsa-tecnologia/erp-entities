/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.erp.ejb.entities.info.Empresa;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "motivo_emision_interno", catalog = "erp", schema = "facturacion")
@XmlRootElement
public class MotivoEmisionInterno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private Character activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_motivo_emision")
    private Integer idMotivoEmision;
    @JoinColumn(name = "id_motivo_emision", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MotivoEmision motivoEmision;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Empresa empresa;

    public MotivoEmisionInterno() {
    }

    public MotivoEmisionInterno(Integer id) {
        this.id = id;
    }

    public MotivoEmisionInterno(Integer id, String codigo, String descripcion) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public MotivoEmision getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(MotivoEmision motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MotivoEmisionInterno)) {
            return false;
        }
        MotivoEmisionInterno other = (MotivoEmisionInterno) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.erp.entities.facturacion.MotivoEmision[ id=" + id + " ]";
    }
    
}
